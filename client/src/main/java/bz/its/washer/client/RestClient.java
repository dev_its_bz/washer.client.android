/*
 * Copyright (c) ITS 2014.
 *
 * www.its.bz
 * dev@its.bz
 */

package bz.its.washer.client;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;

/**
 * Created by zolotarev on 21.03.14.
 */
public class RestClient {
    private static AsyncHttpClient mClient = new AsyncHttpClient();
    private static Activity mActivity;
    private static String mBaseUrl;
    private static boolean mKillActivityOnFailure = true;
    private static Context mContext;

    public RestClient(Activity activity, boolean killActivityOnFailure) {
        mActivity = activity;
        mContext = mActivity.getBaseContext();
        mBaseUrl = mActivity.getString(R.string.baseApiUrl);
        mClient.setMaxRetriesAndTimeout(5, 10000);
        mKillActivityOnFailure = killActivityOnFailure;
    }

    public RestClient(Activity activity) {
        mActivity = activity;
        mContext = mActivity.getBaseContext();
        mClient.setMaxRetriesAndTimeout(5, 10000);
    }

    private static RequestParams addAuthParams(RequestParams params) {
        SharedPreferences pref = mActivity.getApplication().getSharedPreferences("bz.its.washer.client", Context.MODE_PRIVATE);
        String auth_polzovatel_id = pref.getString("auth_polzovatel_id", "");
        String auth_polzovatel_parol = pref.getString("auth_polzovatel_parol", "");
        params.add("auth_polzovatel_id", auth_polzovatel_id);
        params.add("auth_polzovatel_parol", auth_polzovatel_parol);
        return params;
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return mBaseUrl + relativeUrl;
    }

    public void get(String url, RequestParams params, RestAsyncHttpResponseHandler responseHandler) {
        responseHandler.mClient = mClient;
        responseHandler.mActivity = mActivity;
        responseHandler.mContext = mContext;
        responseHandler.mKillActivityOnFailure = mKillActivityOnFailure;

        params = addAuthParams(params);
        mClient.get(mContext, getAbsoluteUrl(url), params, responseHandler);
        Log.v("bz.its.washer.client", url + "?" + params.toString());
    }

    public void post(String url, RequestParams params, RestAsyncHttpResponseHandler responseHandler) {
        responseHandler.mClient = mClient;
        responseHandler.mActivity = mActivity;
        responseHandler.mContext = mContext;
        responseHandler.mKillActivityOnFailure = mKillActivityOnFailure;

        params = addAuthParams(params);
        mClient.post(mContext, getAbsoluteUrl(url), params, responseHandler);
        Log.v("bz.its.washer.client", url + "?" + params.toString());
    }
}