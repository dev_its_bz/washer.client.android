/*
 * Copyright (c) ITS 2014.
 *
 * www.its.bz
 * dev@its.bz
 */

package bz.its.washer.client;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;


public class EditProfileActivity extends Activity {

    private EditText profileName;
    private EditText profileEmail;
    private EditText profilePhone;

    public EditProfileActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.activity_edit_profile);

        profileName = (EditText) findViewById(R.id.profileName);
        profileEmail = (EditText) findViewById(R.id.profileEmail);
        profilePhone = (EditText) findViewById(R.id.user_login);
        profilePhone.addTextChangedListener(
                new MaskedWatcher("+# (###) ###-##-##")
        );

        RequestParams params = new RequestParams();
        params.add("model", "polzovatel");
        RestClient restClient = new RestClient(this);
        restClient.post("view", params, new RestAsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                super.onSuccess(statusCode, headers, responseBody);
                try {
                    JSONObject profile = data.getJSONArray("data").getJSONObject(0);
                    profileName.setText(profile.getString("fio"));
                    profileEmail.setText(profile.getString("email"));
                    profilePhone.setText(profile.getString("telefon"));
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), getString(R.string.message_error_ocured) + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();//go back to the previous Activity
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void onClickBack(View v) {
        finish();//go back to the previous Activity
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void onClickSave(View v) {
        RequestParams params = new RequestParams();
        params.add("model", "polzovatel");
        params.put("fio", profileName.getText().toString());
        params.put("email", profileEmail.getText().toString());
        RestClient restClient = new RestClient(this);
        restClient.post("update", params, new RestAsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Intent intent = new Intent();
                intent.putExtra("userSave", true);
                setResult(RESULT_OK, intent);
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
        });
    }

    public void onClickLogoff(View v) {
        SharedPreferences pref = Utils.getPref(getBaseContext());
        SharedPreferences.Editor prefs = pref.edit();
        prefs.clear();
        prefs.apply();

        Intent intent = new Intent();
        intent.putExtra("doLogoff", "1");
        setResult(RESULT_OK, intent);

        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
