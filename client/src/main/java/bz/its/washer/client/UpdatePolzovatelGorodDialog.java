package bz.its.washer.client;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import java.util.ArrayList;
import java.util.Map;

public class UpdatePolzovatelGorodDialog extends AlertDialog.Builder {

    protected UpdatePolzovatelGorodDialog(Context context, final String gorod, final String gorod_id, final Map gorodIdList, final Activity mActivity, final ArrayList<String> gorodList) {
        super(context);

        this.setTitle(mActivity.getString(R.string.choose_city_title));
        this.setIcon(R.drawable.ic_location_place_big);
        this.setMessage(mActivity.getString(R.string.you_are_in) + gorod + "?");
        this.setNegativeButton(mActivity.getString(R.string.no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        showGorodListDialod(mActivity, gorodList, gorodIdList);
                    }
                }
        );
        this.setPositiveButton(mActivity.getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ((MainActivity) mActivity).updatePolzovatelGorod(gorod_id);
                dialog.cancel();
            }
        });
    }

    public void showGorodListDialod(final Activity mActivity, final ArrayList<String> gorodList, final Map gorodIdList) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle(mActivity.getString(R.string.choose_city_title));
        builder.setCancelable(false);
        builder.setIcon(R.drawable.ic_location_place_big);
        final String[] items_town = gorodList.toArray(new String[gorodList.size()]);
        builder.setItems(items_town, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                String gorod_id = (String) gorodIdList.get(items_town[which]);
                ((MainActivity) mActivity).updatePolzovatelGorod(gorod_id);
            }
        });
        builder.show();
    }
}