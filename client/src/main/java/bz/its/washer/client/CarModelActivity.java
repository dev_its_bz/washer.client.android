/*
 * Copyright (c) ITS 2014.
 *
 * www.its.bz
 * dev@its.bz
 */

package bz.its.washer.client;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import org.json.JSONException;


public class CarModelActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.activity_car_model);

        AdapterView.OnItemClickListener onCarModelClick = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                TextView carModel = (TextView) view.findViewById(R.id.car_model_nazvanie);
                TextView carModelId = (TextView) view.findViewById(R.id.car_model_id);
                Intent intent = new Intent();
                intent.putExtra("carModel", carModel.getText().toString());
                intent.putExtra("carModelId", carModelId.getText().toString());
                setResult(RESULT_OK, intent);
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
        };

        RequestParams params = new RequestParams();
        String carMarkaId = getIntent().getStringExtra("car_marka_id");
        params.add("car_marka_id", carMarkaId);
        RestList carList = new RestList(this, null, R.id.carModelListView, R.layout.list_item_car_models, onCarModelClick);
        try {
            carList.restLoadList("car_model", params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        finish();//go back to the previous Activity
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void onClickBack(View v) {
        finish();//go back to the previous Activity
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
