/*
 * Copyright (c) ITS 2014.
 *
 * www.its.bz
 * dev@its.bz
 */

package bz.its.washer.client;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;
import com.loopj.android.image.SmartImageView;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by zolotarev on 21.03.14.
 */
public class RestList {

    private int mListViewResId, mListItemResId;
    private Activity mActivity;
    private View mView;
    private AdapterView.OnItemClickListener mOnItemClickListener;

    public RestList(Activity activity, View view, int listViewResId, int listItemResId, AdapterView.OnItemClickListener onItemClickListener) {
        mListViewResId = listViewResId;
        mListItemResId = listItemResId;
        mActivity = activity;
        if (view != null)
            mView = view;
        else
            mView = activity.getWindow().getDecorView();
        mOnItemClickListener = onItemClickListener;
    }

    public void restLoadList(String model, RequestParams params) throws JSONException {
        params.add("model", model);
        RestClient restClient = new RestClient(mActivity, true);
        restClient.post("list", params, new RestAsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        super.onSuccess(statusCode, headers, responseBody);
                        ListView mListView = (ListView) mView.findViewById(mListViewResId);
                        try {
                            mListView.setEmptyView(mView.findViewById(android.R.id.empty));
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                        if (data == null)
                            return;


                        //Пробуем получить массив
                        JSONArray records = null;
                            try {
                                records = data.getJSONArray("data");
                            } catch (JSONException e) {
                                e.printStackTrace();
                                mListView.setAdapter(null);
                                return;
                            }

                        //ФОрмируем карту элементов массива данных для листа
                        JSONObject record = new JSONObject();
                        ArrayList<HashMap<String, String>> arrayMap = new ArrayList<HashMap<String, String>>();
                        for (int i = 0; i < records.length(); i++) {
                            try {
                                record = records.getJSONObject(i);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            HashMap<String, String> map = new HashMap<String, String>();
                            Iterator keys = record.keys();
                            while (keys.hasNext()) {
                                String key = (String) keys.next();
                                try {
                                    map.put(key, record.getString(key));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            arrayMap.add(map);
                        }

                        //Формируем массивы имён полей и ищем идентификаторы элементов View для связки
                        List<String> listKeys = new ArrayList<String>();
                        List<Integer> listViews = new ArrayList<Integer>();

                        int i = 0;
                        Iterator keys = record.keys();
                        while (keys.hasNext()) {
                            String key = (String) keys.next();
                            int resID = mActivity.getResources().getIdentifier(key, "id", "bz.its.washer.client");

                            if (resID != 0) {
                                listKeys.add(key);
                                listViews.add(resID);
                                i++;
                            }
                        }
                        String[] arrKeys = new String[listKeys.size()];
                        listKeys.toArray(arrKeys);

                        int[] arrRes = new int[listViews.size()];
                        for (int k = 0; k < listViews.size(); k++)
                            arrRes[k] = listViews.get(k);

                        class SelectionAdapter extends SimpleAdapter {
                            public SelectionAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {
                                super(context, data, resource, from, to);
                            }
                        }

                        SelectionAdapter adapter = new SelectionAdapter(mActivity, arrayMap,
                                mListItemResId, arrKeys, arrRes);
                        adapter.setViewBinder(new SelectionAdapter.ViewBinder() {
                            @Override
                            public boolean setViewValue(View view, Object o, String s) {
                                if (view.getId() == R.id.drawable) {
                                    ((ImageView) view).setImageResource(Utils.getId(s, R.drawable.class));
                                    return true; //true because the data was bound to the view
                                }
                                else
                                if (view.getId() == R.id.grade) {
                                    ((RatingBar) view).setRating(Float.parseFloat(s));
                                    return true; //true because the data was bound to the view
                                }

                                if (view.getId() == R.id.image) {
                                    ((SmartImageView) view).setImageUrl(mActivity.getString(R.string.baseSiteUrl)+"images/" + s);
                                    return true; //true because the data was bound to the view
                                }
                                return false;
                            }
                        });


                        mListView.setAdapter(adapter);
                        mListView.setOnItemClickListener(mOnItemClickListener);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        super.onFailure(statusCode, headers, responseBody, error);
                    }
                }
        );
    }
}
