/*
 * Copyright (c) ITS 2014.
 *
 * www.its.bz
 * dev@its.bz
 */

package bz.its.washer.client;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

import java.util.ArrayList;


public class SelectUslugaActivity extends Activity {

    RestServicesList SList;

    private void selectItems(ArrayList<String> listItemsId) {
        SList.unsetAllItems();
        if (listItemsId.size() > 0)
            for (String id : listItemsId) {
                SList.selectItem(Integer.valueOf(id));
            }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.activity_select_usluga_list);

        SList = new RestServicesList(this, (ExpandableListView) findViewById(R.id.uslugi_tree_list));
        SList.DrawList();
    }

    public void onClickClear(View v) {
        SList.unsetAllItems();
        SList.adapter.notifyDataSetChanged();
        Toast.makeText(getBaseContext(), getString(R.string.services_are_cleared), Toast.LENGTH_LONG).show();
    }

    public void onClickPreset(View v) {
        ArrayList<String> items = new ArrayList<String>();
        items.add("10");
        items.add("12");
        items.add("15");
        selectItems(items);
        Toast.makeText(getBaseContext(), getString(R.string.typical_services_are_selected), Toast.LENGTH_LONG).show();
    }

    public void onClickSave(View v) {
        Intent intent = new Intent();

        ArrayList uslugi_id = SList.GetCheckedElementsId();
        ArrayList uslugi_name = SList.GetCheckedElementsNames();

        intent.putExtra("uslugiSelect", true);

        SharedPreferences pref = Utils.getPref(getBaseContext());
        SharedPreferences.Editor prefs = pref.edit();

        prefs.putString("uslugi_id_filter", uslugi_id.size()>0?uslugi_id.toString():"");
        prefs.putString("uslugi_name_filter", uslugi_name.toString());

        prefs.apply();

        setResult(RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }


    public void onClickBack(View v) {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

}
