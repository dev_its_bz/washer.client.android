package bz.its.washer.client;

/**
 * Created by Пользователь on 14.05.2014.
 */

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.bugsense.trace.BugSenseHandler;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public class GcmIntentService extends IntentService {
    public static final int NOTIFICATION_ID = 1;
    private static final String TAG = "GcmIntentService";
    private static final int WASH_END = 1;
    private static final int STATUS_CHANGED = 2;
    private static final int SIMPLE_MESSAGE = 3;
    private static final int NEED_UPDATE = 4;
    NotificationCompat.Builder builder;
    private NotificationManager mNotificationManager;

    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);


        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM
             * will be extended in the future with new message types, just ignore
             * any message types you're not interested in, or that you don't
             * recognize.
             */
            if (GoogleCloudMessaging.
                    MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                sendNotification("Send error: " + extras.toString(), 0, null);
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_DELETED.equals(messageType)) {
                sendNotification("Deleted messages on server: " +
                        extras.toString(), 0, null);
                // If it's a regular GCM message, do some work.
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                // This loop represents the service doing some work.
                int action = Integer.parseInt(extras.getString("action"));

                JSONObject jsonObject = null;
                if (extras.getString("params") != null) {  //если есть параметры получаем их
                    try {
                        jsonObject = new JSONObject(extras.getString("params"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


                sendNotification(extras.getString("Notice"), action, jsonObject);
                Log.i(TAG, "Received: " + extras.toString());
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReciever.completeWakefulIntent(intent);
    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendNotification(String msg, int action, JSONObject params) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);
        //int notify_id = UUID.randomUUID().hashCode(); //??? не работает ???

        PendingIntent contentIntent = null;
        switch (action) {
            case WASH_END:
                Intent wash_end = new Intent(this, WashEndActivity.class);
                SetParamsToIntent(wash_end, params /*, notify_id*/);
                contentIntent = PendingIntent.getActivity(this, 0, wash_end, 0);
                break;
            case STATUS_CHANGED:
                contentIntent = PendingIntent.getActivity(this, 0,
                        new Intent(this, MainActivity.class), 0);
                break;
            case SIMPLE_MESSAGE:
                contentIntent = PendingIntent.getActivity(this, 0,
                        new Intent(this, MainActivity.class), 0);
                break;
            case NEED_UPDATE:
                contentIntent = PendingIntent.getActivity(this, 0,
                        new Intent(this, MainActivity.class), 0);
                break;
            case 0:
                contentIntent = PendingIntent.getActivity(this, 0,
                        new Intent(this, MainActivity.class), 0);
                break;
        }


        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        long[] pattern = {500,500,500,500,500,500,500,500,500};

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        // .setSmallIcon(R.drawable.ic_stat_gcm)
                        .setContentTitle(getString(R.string.app_name))
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setContentText(msg)
                        .setLights(Color.BLUE, 500, 500)
                        .setVibrate(pattern)
                        .setStyle(new NotificationCompat.InboxStyle())
                        .setSound(alarmSound);

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());


    }

    private void SetParamsToIntent(Intent mIntent, JSONObject params /*, int notify_id*/) {
        Iterator keys = params.keys();

        while (keys.hasNext()) {
            String key = (String) keys.next();
            try {
                mIntent.putExtra(key, params.getString(key));
            } catch (JSONException e) {
                e.printStackTrace();
                BugSenseHandler.sendException(e);
            }
        }
    }
}