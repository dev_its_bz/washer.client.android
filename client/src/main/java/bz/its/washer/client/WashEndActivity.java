package bz.its.washer.client;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import org.apache.http.Header;


public class WashEndActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wash_end);
    }

    public void onClickAddReview(View v) {
        EditText text = (EditText) findViewById(R.id.review_textt);
        RatingBar rating = (RatingBar) findViewById(R.id.review_rating);
        RequestParams params = new RequestParams();
        if (((CheckBox) findViewById(R.id.cbAnon)).isChecked())
            params.add("anonym", "1");
        params.add("object_id", getIntent().getStringExtra("object_id"));
        params.add("comment", text.getText().toString());
        params.add("grade", String.valueOf(rating.getRating()));
        params.add("model", "review");

        //TODO: Получать и отправлять с привязкой к отзыву конкретную запись (для отслеживания негативных отзывов к конкретным мойщикам)

        RestClient restClient = new RestClient(this);
        restClient.post("create", params, new RestAsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                super.onSuccess(statusCode, headers, responseBody);
                Toast.makeText(WashEndActivity.this, getString(R.string.THANKYOU), Toast.LENGTH_SHORT).show();
                NotificationManager mNotificationManager = (NotificationManager)
                        WashEndActivity.this.getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.cancel(1);
                WashEndActivity.this.finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void onClickBack(View v) {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
