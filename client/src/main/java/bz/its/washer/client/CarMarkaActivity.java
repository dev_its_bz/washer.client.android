/*
 * Copyright (c) ITS 2014.
 *
 * www.its.bz
 * dev@its.bz
 */

package bz.its.washer.client;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import org.json.JSONException;


public class CarMarkaActivity extends Activity {

    private String carMarka, carMarkaId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.activity_car_marka);

        AdapterView.OnItemClickListener onCarMarkaClick = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                TextView carMarkaText = (TextView) view.findViewById(R.id.car_marka_nazvanie);
                TextView carMarkaIdText = (TextView) view.findViewById(R.id.car_marka_id);
                carMarka = carMarkaText.getText().toString();
                carMarkaId = carMarkaIdText.getText().toString();

                Intent intent = new Intent(CarMarkaActivity.this, CarModelActivity.class);
                intent.putExtra("car_marka_id", carMarkaId);
                startActivityForResult(intent, 1);
            }
        };

        RequestParams params = new RequestParams();
        RestList carList = new RestList(this, null, R.id.carMarkaListView, R.layout.list_item_car_marka, onCarMarkaClick);
        try {
            carList.restLoadList("car_marka", params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            return;
        }
        String carModel = data.getStringExtra("carModel");
        String carModelId = data.getStringExtra("carModelId");

        Intent intent = new Intent();
        intent.putExtra("carMarka", carMarka);
        intent.putExtra("carMarkaId", carMarkaId);

        intent.putExtra("carModel", carModel);
        intent.putExtra("carModelId", carModelId);
        setResult(RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void onBackPressed() {
        finish();//go back to the previous Activity
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void onClickBack(View v) {
        finish();//go back to the previous Activity
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
