/*
 * Copyright (c) ITS 2014.
 *
 * www.its.bz
 * dev@its.bz
 */

package bz.its.washer.client;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

public class SlidesActivity extends Activity {

    static ViewPager mViewPager = null;
    static ImageView step1;
    static ImageView step2;
    static ImageView step3;
    static ImageView step4;
    static ImageView nextButton;
    static ImageView backButton;
    static LinearLayout steps;
    static Button doneButton;
    static int passive_id;
    static int active_id;
    List<View> pages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_slides);

        backButton = (ImageView) findViewById(R.id.wizard_back_button);
        nextButton = (ImageView) findViewById(R.id.wizard_forward_button);
        steps = (LinearLayout) findViewById(R.id.wizard_steps_view);
        step1 = (ImageView) findViewById(R.id.wizard_step_1);
        step2 = (ImageView) findViewById(R.id.wizard_step_2);
        step3 = (ImageView) findViewById(R.id.wizard_step_3);
        step4 = (ImageView) findViewById(R.id.wizard_step_4);
        doneButton = (Button) findViewById(R.id.wizard_done_button);

        passive_id = getResources().getIdentifier("bz.its.washer.client:drawable/ic_wizard_dot", null, null);
        active_id = getResources().getIdentifier("bz.its.washer.client:drawable/ic_wizard_dot_active", null, null);

        LayoutInflater inflater = LayoutInflater.from(this);
        pages = new ArrayList<View>();

        View page = inflater.inflate(R.layout.fragment_slides_1, null);
        pages.add(page);

        page = inflater.inflate(R.layout.fragment_slides_2, null);
        pages.add(page);

        page = inflater.inflate(R.layout.fragment_slides_3, null);
        pages.add(page);

        page = inflater.inflate(R.layout.fragment_slides_4, null);
        pages.add(page);

        PagerAdapterMy pagerAdapter = new PagerAdapterMy(pages);
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(pagerAdapter);
        mViewPager.setCurrentItem(0);
        PageListener pageListener = new PageListener();
        mViewPager.setOnPageChangeListener(pageListener);
    }

    public void onClickNext(View view) {
        if (mViewPager.getCurrentItem() < 5)
            mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1, true);

        Log.v("bz.its.washer.client", String.valueOf(mViewPager.getCurrentItem()));

        switch (mViewPager.getCurrentItem()) {
            case 0:
                backButton.setVisibility(View.INVISIBLE);
                step1.setImageResource(active_id);
                step2.setImageResource(passive_id);
                step3.setImageResource(passive_id);
                step4.setImageResource(passive_id);
                break;
            case 1:
                backButton.setVisibility(View.VISIBLE);
                step1.setImageResource(passive_id);
                step2.setImageResource(active_id);
                step3.setImageResource(passive_id);
                step4.setImageResource(passive_id);
                break;
            case 2:
                step1.setImageResource(passive_id);
                step2.setImageResource(passive_id);
                step3.setImageResource(active_id);
                step4.setImageResource(passive_id);
                nextButton.setVisibility(View.VISIBLE);
                break;
            case 3:
                nextButton.setVisibility(View.INVISIBLE);
                break;
        }
    }

    public void onClickPrev(View view) {
        if (mViewPager.getCurrentItem() > 0)
            mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1, true);

        switch (mViewPager.getCurrentItem()) {
            case 0:
                backButton.setVisibility(View.INVISIBLE);
                step1.setImageResource(active_id);
                step2.setImageResource(passive_id);
                step3.setImageResource(passive_id);
                step4.setImageResource(passive_id);
                break;
            case 1:
                backButton.setVisibility(View.VISIBLE);
                step1.setImageResource(passive_id);
                step2.setImageResource(active_id);
                step3.setImageResource(passive_id);
                step4.setImageResource(passive_id);
                break;
            case 2:
                step1.setImageResource(passive_id);
                step2.setImageResource(passive_id);
                step3.setImageResource(active_id);
                step4.setImageResource(passive_id);
                nextButton.setVisibility(View.VISIBLE);
                break;
            case 3:
                nextButton.setVisibility(View.INVISIBLE);
                break;
        }
    }

    public void onClickDone(View view) {
        onClickBack(view);
    }

    @Override
    public void onBackPressed() {
        finish();//go back to the previous Activity
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void onClickBack(View v) {
        finish();//go back to the previous Activity
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private static class PageListener extends ViewPager.SimpleOnPageChangeListener {
        public void onPageSelected(int position) {
            Log.v("bz.its.washer.client", "page selected " + position);
            switch (position) {
                case 0:
                    backButton.setVisibility(View.INVISIBLE);
                    step1.setImageResource(active_id);
                    step2.setImageResource(passive_id);
                    step3.setImageResource(passive_id);
                    step4.setImageResource(passive_id);
                    break;
                case 1:
                    backButton.setVisibility(View.VISIBLE);
                    step1.setImageResource(passive_id);
                    step2.setImageResource(active_id);
                    step3.setImageResource(passive_id);
                    step4.setImageResource(passive_id);
                    break;
                case 2:
                    step1.setImageResource(passive_id);
                    step2.setImageResource(passive_id);
                    step3.setImageResource(active_id);
                    step4.setImageResource(passive_id);
                    nextButton.setVisibility(View.VISIBLE);
                    break;
                case 3:
                    step1.setImageResource(passive_id);
                    step2.setImageResource(passive_id);
                    step3.setImageResource(passive_id);
                    step4.setImageResource(active_id);
                    nextButton.setVisibility(View.INVISIBLE);
                    break;
            }
        }
    }
}
