/*
 * Copyright (c) ITS 2014.
 *
 * www.its.bz
 * dev@its.bz
 */

package bz.its.washer.client;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bugsense.trace.BugSenseHandler;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.loopj.android.http.RequestParams;
import com.newrelic.agent.android.NewRelic;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class MainActivity extends Activity {

    public String resource_id = "";
    public String resource_vremya;
    public String resource_data;
    UpdatePolzovatelGorodDialog td1 = null;
    AlertDialog.Builder td2 = null;
    ProgressDialog pd = null;

    private void getUslugiFromPref() {
        TextView uslugi_empty = (TextView) findViewById(R.id.uslugi_empty);
        TextView cena_text = (TextView) findViewById(R.id.cena);
        TextView dlitelnost_text = (TextView) findViewById(R.id.dlitelnost);
        LinearLayout uslugi_detali = (LinearLayout) findViewById(R.id.uslugi_detali);

        SharedPreferences pref = Utils.getPref(getBaseContext());
        if (pref.getString("uslugi_name", "").length() > 2) {
            uslugi_detali.setVisibility(View.VISIBLE);
            uslugi_empty.setVisibility(View.GONE);
            cena_text.setText(pref.getString("cena", "") + getString(R.string.currency_short));
            dlitelnost_text.setText(pref.getString("dlitelnost", "") + getString(R.string.time_short));
        } else {
            uslugi_detali.setVisibility(View.GONE);
            uslugi_empty.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NewRelic.withApplicationToken(getString(R.string.new_relic_api_token)).start(this.getApplication());
        BugSenseHandler.initAndStartSession(this, "217e825b");

        SharedPreferences pref = Utils.getPref(getBaseContext());

        String auth_polzovatel_id = pref.getString("auth_polzovatel_id", "");
        String auth_polzovatel_hash = pref.getString("auth_polzovatel_hash", "");

        if (auth_polzovatel_id.contentEquals("") || auth_polzovatel_hash.contentEquals("")) {
            Intent loginActivity = new Intent(getApplicationContext(), LoginActivity.class);
            finish();
            startActivity(loginActivity);
            return;
        }
        setContentView(R.layout.activity_main);

        if (!pref.contains("first_main")) {
            pref.edit().putString("first_main", "1").apply();
            //================
            final Dialog dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.coach_mark_main);
            dialog.setCanceledOnTouchOutside(true);
            //for dismissing anywhere you touch
            View masterView = dialog.findViewById(R.id.coach_mark_master_view);
            masterView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            dialog.show();
            //====================
        }

        pref = Utils.getPref(getBaseContext());
        TextView polzovatelCar = (TextView) findViewById(R.id.polzovatel_car);
        polzovatelCar.setText(pref.getString("car_name", getString(R.string.not_selected)));

        TextView polzovatel_object = (TextView) findViewById(R.id.polzovatel_object);
        polzovatel_object.setText(pref.getString("object_name", getString(R.string.not_selected)));

        getUslugiFromPref();
        getZapisiActiveCount();

        LocationListener mLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                SharedPreferences pref = Utils.getPref(getBaseContext());
                SharedPreferences.Editor prefs = pref.edit();

                String myLat = String.valueOf(location.getLatitude());
                String myLon = String.valueOf(location.getLongitude());

                prefs.putString("myLat", myLat);
                prefs.putString("myLon", myLon);
                prefs.putBoolean("LocationOn", true);
                prefs.apply();


                //Пишем координаты пользователя в БД
                RequestParams params = new RequestParams();
                params.add("model", "polzovatel_coords");
                params.add("myLat", myLat);
                params.add("myLon", myLon);
                RestClient restClient = new RestClient(MainActivity.this, false);
                restClient.post("update", params, new RestAsyncHttpResponseHandler());

                String gorod_id = pref.getString("gorod_id", "");
                String gorod_lat = pref.getString("gorod_lat", "");
                String gorod_lon = pref.getString("gorod_lon", "");

                double distance = 0;
                try {
                    distance = Utils.getDistance(Float.parseFloat(myLat), Float.parseFloat(myLon), Float.parseFloat(gorod_lat), Float.parseFloat(gorod_lon));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (gorod_id.contentEquals("") || distance > 50)
                    getGorod(myLat, myLon);
                else {
                    updatePolzovatelGorod(gorod_id);
                }

                try {
                    pd.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {
                Toast.makeText(getBaseContext(), getString(R.string.cant_resolve_geolocation), Toast.LENGTH_LONG).show();
                SharedPreferences pref = Utils.getPref(getBaseContext());
                SharedPreferences.Editor prefs = pref.edit();
                prefs.putBoolean("LocationOn", false);
                prefs.apply();
                String myLat = pref.getString("myLat", "");
                String myLon = pref.getString("myLon", "");
                try {
                    pd.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getGorod(myLat, myLon);
            }
        };

        pd = new ProgressDialog(this);
        pd.setMessage(getString(R.string.geolocation_in_progress));
        pd.setIndeterminate(true);
        pd.setProgressDrawable(this.getResources().getDrawable(R.drawable.apptheme_progress_indeterminate_horizontal_holo_light));
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);

        try {
            pd.show();
            LocationManager mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 60000, 100, mLocationListener);
        } catch (Exception e) {
            Toast.makeText(getBaseContext(), getString(R.string.cant_resolve_geolocation), Toast.LENGTH_LONG).show();
            pref = Utils.getPref(getBaseContext());
            SharedPreferences.Editor prefs = pref.edit();
            prefs.putBoolean("LocationOn", false);
            prefs.apply();
            String myLat = pref.getString("myLat", "");
            String myLon = pref.getString("myLon", "");
            try {
                pd.dismiss();
            } catch (Exception er) {
                er.printStackTrace();
            }
            getGorod(myLat, myLon);
        }


        if (checkPlayServices()) {
            GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
            String regid = Utils.getPref(getBaseContext()).getString("registration_id", "");
            if (regid.isEmpty()) {
                new RegisterApp(getApplicationContext(), gcm, this).execute();
            }
        }
    }

    public void updatePolzovatelGorod(String gorod_id) {

        RequestParams params = new RequestParams();
        params.add("model", "polzovatel_gorod");
        params.add("gorod_id", gorod_id);
        RestClient restClient = new RestClient(this, false);
        restClient.post("update", params, new RestAsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                super.onSuccess(statusCode, headers, responseBody);

                String lat = null;
                String lon = null;
                String gorod = "";
                String gorodId = "";
                String gorodLat = "";
                String gorodLon = "";
                String gorodPogoda = null;
                try {
                    lat = data.getJSONObject("data").getString("lat");
                    lon = data.getJSONObject("data").getString("lon");
                    gorod = data.getJSONObject("data").getString("nazvanie");
                    gorodId = data.getJSONObject("data").getString("id");
                    gorodLat = data.getJSONObject("data").getString("lat");
                    gorodLon = data.getJSONObject("data").getString("lon");
                    gorodPogoda = data.getJSONObject("data").getString("pogoda");
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                SharedPreferences pref = getApplication().getSharedPreferences("bz.its.washer.client", Context.MODE_PRIVATE);
                SharedPreferences.Editor prefs = pref.edit();
                if (!gorodId.contentEquals(pref.getString("gorod_id", ""))) {
                    prefs.putString("gorod_id", gorodId);
                    prefs.putString("gorod", gorod);
                    prefs.putString("gorod_lat", gorodLat);
                    prefs.putString("gorod_lon", gorodLon);

                    prefs.remove("object_id");
                    prefs.remove("object_name");
                    prefs.remove("uslugi_id");
                    prefs.remove("uslugi_name");
                    prefs.apply();
                    resource_id = "";
                    showSelectedResource();
                    pref.edit().remove("object_id").apply();
                }

                prefs.putString("gorod_pogoda", gorodPogoda);
                prefs.putBoolean("LocationOn", true);
                prefs.apply();

                if (pref.getString("myLat", "").contentEquals("")) {
                    prefs.putString("myLat", lat);
                    prefs.putString("myLon", lon);
                    prefs.apply();
                }

                object_active_count(gorodId);

                try {
                    showGorodPogoda();
                    getZapisiActiveCount();
                    if (!pref.getString("car_id", "").contentEquals("")) {
                        getFastChoose();
                    } else
                        Toast.makeText(mContext, getString(R.string.choose_car), Toast.LENGTH_LONG).show();
                } catch (RuntimeException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void showGorodPogoda() {
        SharedPreferences pref = Utils.getPref(this);
        LinearLayout gorod_pogoda = (LinearLayout) findViewById(R.id.gorod_pogoda);
        if (!pref.getString("gorod_pogoda", "").contentEquals("")) {
            try {
                JSONArray pogoda_list = new JSONArray(pref.getString("gorod_pogoda", ""));
                for (int i = 0; i < pogoda_list.length(); i++) {
                    switch (i) {
                        case 0:
                            ((ImageView) findViewById(R.id.gorod_pogoda_1)).setImageResource(getResources().getIdentifier("ic_weather_" + pogoda_list.getJSONObject(i).getJSONArray("weather").getJSONObject(0).getString("icon"), "drawable", getPackageName()));
                            break;
                        case 1:
                            ((ImageView) findViewById(R.id.gorod_pogoda_2)).setImageResource(getResources().getIdentifier("ic_weather_" + pogoda_list.getJSONObject(i).getJSONArray("weather").getJSONObject(0).getString("icon"), "drawable", getPackageName()));
                            break;
                        case 2:
                            ((ImageView) findViewById(R.id.gorod_pogoda_3)).setImageResource(getResources().getIdentifier("ic_weather_" + pogoda_list.getJSONObject(i).getJSONArray("weather").getJSONObject(0).getString("icon"), "drawable", getPackageName()));
                            break;
                        case 3:
                            ((ImageView) findViewById(R.id.gorod_pogoda_4)).setImageResource(getResources().getIdentifier("ic_weather_" + pogoda_list.getJSONObject(i).getJSONArray("weather").getJSONObject(0).getString("icon"), "drawable", getPackageName()));
                            break;
                        case 4:
                            ((ImageView) findViewById(R.id.gorod_pogoda_5)).setImageResource(getResources().getIdentifier("ic_weather_" + pogoda_list.getJSONObject(i).getJSONArray("weather").getJSONObject(0).getString("icon"), "drawable", getPackageName()));
                            break;
                        case 5:
                            ((ImageView) findViewById(R.id.gorod_pogoda_6)).setImageResource(getResources().getIdentifier("ic_weather_" + pogoda_list.getJSONObject(i).getJSONArray("weather").getJSONObject(0).getString("icon"), "drawable", getPackageName()));
                            break;
                        case 6:
                            ((ImageView) findViewById(R.id.gorod_pogoda_7)).setImageResource(getResources().getIdentifier("ic_weather_" + pogoda_list.getJSONObject(i).getJSONArray("weather").getJSONObject(0).getString("icon"), "drawable", getPackageName()));
                            break;
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            gorod_pogoda.setVisibility(View.VISIBLE);
        } else
            gorod_pogoda.setVisibility(View.GONE);
    }

    private void object_active_count(String gorodId) {
        RequestParams params = new RequestParams();
        params.add("model", "object_active_count");
        params.add("gorod_id", gorodId);

        RestClient restClient = new RestClient(this, false);
        restClient.post("view", params,
                new RestAsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        super.onSuccess(statusCode, headers, responseBody);

                        if (data != null) {
                            try {
                                JSONObject info = data.getJSONObject("data");
                                ((TextView) findViewById(R.id.gorode)).setText(info.getString("gorode"));
                                ((TextView) findViewById(R.id.object_count)).setText(info.getString("objects_count"));
                                String avtomoek = "";
                                Utils.getPref(getBaseContext()).edit().putString("gorode", info.getString("gorode"));
                                Integer count = Integer.parseInt(info.getString("objects_count"));
                                if (count == 0)
                                    avtomoek = getString(R.string.car_washer_in_0);
                                else {
                                    count = count % 10;
                                    if (count == 1)
                                        avtomoek = getString(R.string.car_washer_in_1);
                                    else if (count == 2 || count == 3 || count == 4)
                                        avtomoek = getString(R.string.car_washer_in_234);
                                    else if (count >= 5 || count == 0)
                                        avtomoek = getString(R.string.car_washer_in_5);
                                }

                                ((TextView) findViewById(R.id.avtomoek)).setText(avtomoek);

                                findViewById(R.id.object_count_footer).setVisibility(View.VISIBLE);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                findViewById(R.id.object_count_footer).setVisibility(View.GONE);
                            }
                        }
                    }
                }
        );
    }

    public void getZapisiActiveCount() {
        //Получаем список активных записей на мойку

        final TextView zapis_info_text = (TextView) findViewById(R.id.zapis_info);
        zapis_info_text.setText(getString(R.string.loading_message));

        RequestParams params = new RequestParams();
        params.add("model", "zapis_active");

        RestClient restClient = new RestClient(MainActivity.this, false);
        restClient.post("list", params,
                new RestAsyncHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        super.onFailure(statusCode, headers, responseBody, error);
                        zapis_info_text.setText(getString(R.string.message_error_ocured) + getString(R.string.try_to_reenter_later));
                        Utils.getPref(getBaseContext()).edit().remove("hasRecords").apply();
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        super.onSuccess(statusCode, headers, responseBody);
                        JSONArray zapisi;

                        if (data != null) {
                            try {
                                zapisi = data.getJSONArray("data");
                            } catch (JSONException e) {
                                e.printStackTrace();
                                zapis_info_text.setText(getString(R.string.error_recieving_record_data));
                                Utils.getPref(getBaseContext()).edit().remove("hasRecords").apply();
                                return;
                            }
                            if (zapisi.length() == 0) {
                                zapis_info_text.setText(getString(R.string.now_you_are_not_booked));
                                Utils.getPref(getBaseContext()).edit().putBoolean("hasRecords", false).apply();
                            } else {
                                String zap;
                                switch (zapisi.length()) {
                                    case 1:
                                        zap = getString(R.string.aktive_record_1);
                                        break;
                                    case 2:
                                    case 3:
                                    case 4:
                                        zap = getString(R.string.active_record_234);
                                        break;
                                    default:
                                        zap = getString(R.string.active_record_5);
                                }
                                zapis_info_text.setText(getString(R.string.you_has) + String.valueOf(zapisi.length()) + zap + "\n" + getString(R.string.to_see));
                                Utils.getPref(getBaseContext()).edit().putBoolean("hasRecords", true).apply();
                            }
                        } else {
                            zapis_info_text.setText(getString(R.string.error_recieving_record_data));
                            Utils.getPref(getBaseContext()).edit().remove("hasRecords").apply();
                        }
                    }
                }
        );
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode == ConnectionResult.SERVICE_MISSING || resultCode == ConnectionResult.SERVICE_DISABLED || resultCode == ConnectionResult.SERVICE_INVALID) {
            if (Utils.getPref(getBaseContext()).getBoolean("isFirstRun", true)) {
                Utils.getPref(getBaseContext()).edit().putBoolean("isFirstRun", false);
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
                dlgAlert.setMessage(getString(R.string.google_play_services_are_not_installed));
                dlgAlert.setTitle(getString(R.string.warning));
                dlgAlert.setPositiveButton(getString(R.string.ok), null);
                dlgAlert.setCancelable(true);
                dlgAlert.create().show();

                if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                    GooglePlayServicesUtil.getErrorDialog(resultCode, this, 9000).show();
                }
            }
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void getGorod(String lat, String lon) {

        RequestParams params = new RequestParams();
        params.add("myLat", lat);
        params.add("myLon", lon);
        params.add("model", "gorod");
        RestClient restClient = new RestClient(MainActivity.this, false);
        restClient.post("list", params,
                new RestAsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        super.onSuccess(statusCode, headers, responseBody);
                        String gorode = null;
                        String gorod_id = null;
                        JSONArray records;
                        Map<String, String> gorod_id_list = new HashMap<String, String>();

                        ArrayList<String> gorod_list = new ArrayList<String>();
                        if (data != null) {
                            try {
                                records = data.getJSONArray("data");
                                gorode = records.getJSONObject(0).getString("nazvanie_tv");
                                gorod_id = records.getJSONObject(0).getString("id");

                                for (int i = 0; i < records.length(); i++) {
                                    gorod_id_list.put(records.getJSONObject(i).getString("nazvanie"), records.getJSONObject(i).getString("id"));
                                    gorod_list.add(records.getJSONObject(i).getString("nazvanie"));
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                return;
                            }
                        }

                        SharedPreferences pref = Utils.getPref(getBaseContext());
                        String myLat = pref.getString("myLat", "");
                        String myLon = pref.getString("myLon", "");

                        if (myLat.equals("") || myLon.equals("")) {
                            if (td1 == null)
                                td1 = new UpdatePolzovatelGorodDialog(MainActivity.this, gorode, gorod_id, gorod_id_list, MainActivity.this, gorod_list);
                            td1.setCancelable(false);
                            td1.showGorodListDialod(MainActivity.this, gorod_list, gorod_id_list);
                        } else {
                            if (td2 == null)
                                td2 = new UpdatePolzovatelGorodDialog(MainActivity.this, gorode, gorod_id, gorod_id_list, MainActivity.this, gorod_list);
                            td2.setCancelable(false);
                            td2.show();
                        }
                    }
                }
        );
    }

    public void onClickSettings(View v) {
        Intent settings = new Intent(getBaseContext(), SettingsActivity.class);
        startActivity(settings);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void onClickSelectCity(View v) {
        SharedPreferences pref = Utils.getPref(getBaseContext());
        String myLat = pref.getString("myLat", "");
        String myLon = pref.getString("myLon", "");
        getGorod(myLat, myLon);
    }

    public void onClickSelectCar(View v) {
        Intent intent = new Intent(this, SelectCarActivity.class);
        startActivityForResult(intent, 1);
    }

    public void onClickSelectObject(View v) {
        if (Utils.getPref(getBaseContext()).getString("gorod_id", "").contentEquals("")) {
            Toast.makeText(getBaseContext(), getString(R.string.choose_city_first), Toast.LENGTH_LONG).show();
            return;
        }
        if (Utils.getPref(getBaseContext()).getString("car_id", "").contentEquals("")) {
            Toast.makeText(getBaseContext(), getString(R.string.choose_car_first), Toast.LENGTH_LONG).show();
            return;
        }

        Intent intent = new Intent(this, ObjectListActivity.class);
        startActivityForResult(intent, 1);
    }

    private boolean validateFields() {
        if (Utils.getPref(getBaseContext()).getString("gorod_id", "").contentEquals("")) {
            Toast.makeText(getBaseContext(), getString(R.string.choose_city), Toast.LENGTH_LONG).show();
            return false;
        }
        if (Utils.getPref(getBaseContext()).getString("car_id", "").contentEquals("")) {
            Toast.makeText(getBaseContext(), getString(R.string.choose_car), Toast.LENGTH_LONG).show();
            return false;
        }

        if (Utils.getPref(getBaseContext()).getString("object_id", "").contentEquals("")) {
            Toast.makeText(getBaseContext(), getString(R.string.choose_carwash), Toast.LENGTH_LONG).show();
            return false;
        }

        if (resource_id.contentEquals("")) {
            Toast.makeText(getBaseContext(), getString(R.string.choose_time), Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    public void onClickEditProfile(View v) {
        Intent intent = new Intent(this, EditProfileActivity.class);
        startActivityForResult(intent, 1);
    }

    public void onClickAbout(View v) {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            return;
        }

        if (data.hasExtra("doLogoff")) {
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            return;
        }

        if (data.hasExtra("carSelect")) {
            TextView polzovatel_car = (TextView) findViewById(R.id.polzovatel_car);
            TextView polzovatel_car_nomer = (TextView) findViewById(R.id.polzovatel_car_nomer);

            if (!Utils.getPref(getBaseContext()).contains("car_id")) {
                polzovatel_car.setText(getString(R.string.not_selected));
                polzovatel_car_nomer.setVisibility(View.GONE);
            } else {
                String car_name = Utils.getPref(getBaseContext()).getString("car_name", "");
                String car_name_nomer = Utils.getPref(getBaseContext()).getString("car_name_nomer", "");
                polzovatel_car.setText(car_name);

                if (car_name_nomer.contentEquals("")) {
                    polzovatel_car_nomer.setVisibility(View.GONE);
                } else {
                    polzovatel_car_nomer.setVisibility(View.VISIBLE);
                    polzovatel_car_nomer.setText(car_name_nomer);
                }
            }
            Utils.getPref(getBaseContext()).edit().remove("object_id").apply();
            getFastChoose();
        }

        if (data.hasExtra("objectSelect")) {
            showSelectedObject();
        }

        if (data.hasExtra("vremyaSelect")) {
            resource_id = data.getStringExtra("resource_id");
            resource_data = data.getStringExtra("resource_data");
            resource_vremya = data.getStringExtra("resource_vremya");
            showSelectedResource();
        }

        if (data.hasExtra("zapisChange")) {
            getZapisiActiveCount();
        }
    }

    public void showSelectedObject() {
        if (!Utils.getPref(getBaseContext()).getString("object_id", "").contentEquals("")) {
            String object_name = Utils.getPref(getBaseContext()).getString("object_name", "");
            TextView polzovatel_object = (TextView) findViewById(R.id.polzovatel_object);
            polzovatel_object.setText(object_name);
            getUslugiFromPref();
        } else {
            TextView polzovatel_object = (TextView) findViewById(R.id.polzovatel_object);
            polzovatel_object.setText(getString(R.string.not_selected));

            SharedPreferences pref = Utils.getPref(getBaseContext());
            pref.edit().remove("uslugi_name").apply();
            pref.edit().remove("uslugi_id").apply();
            getUslugiFromPref();
        }
        resource_id = "";
        showSelectedResource();
    }

    public void showSelectedResource() {
        if (!resource_id.contentEquals("")) {
            TextView polzovatel_vremya = (TextView) findViewById(R.id.polzovatel_vremya);
            polzovatel_vremya.setText(resource_data + " в " + resource_vremya);

        } else {
            ((TextView) findViewById(R.id.polzovatel_vremya)).setText(getString(R.string.not_selected));
            resource_data = "";
            resource_vremya = "";
        }
    }

    public void onClickCreateZapis(View view) {
        if (!validateFields()) return;
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        doCreateZapis();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.want_to_booking)).setPositiveButton(getString(R.string.yes), dialogClickListener)
                .setNegativeButton(getString(R.string.no), dialogClickListener).show();
    }

    private void doCreateZapis() {
        SharedPreferences pref;
        String car_id;
        String uslugi_id;
        String cena;
        String dlitelnost;

        pref = Utils.getPref(getBaseContext());
        car_id = pref.getString("car_id", "");
        uslugi_id = pref.getString("uslugi_id", "");
        cena = pref.getString("cena", "");
        dlitelnost = pref.getString("dlitelnost", "");

        RequestParams params = new RequestParams();
        params.add("car_id", car_id);
        params.add("resource_id", resource_id);

        if (uslugi_id.length() > 2) {
            uslugi_id = uslugi_id.substring(1, uslugi_id.length() - 1);
            params.put("uslugi_id", uslugi_id);
            params.add("cena", cena);
            params.add("dlitelnost", dlitelnost);
        }
        params.put("model", "zapis");
        RestClient restClient = new RestClient(this);
        restClient.post("create", params, new RestAsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                super.onSuccess(statusCode, headers, responseBody);
                String msg;
                try {
                    msg = data.getString("data");
                } catch (JSONException e) {
                    e.printStackTrace();
                    return;
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                if (msg.equals("1")) {
                    builder.setTitle(getString(R.string.warning))
                            .setMessage(getString(R.string.choosen_time_already_busy))
                            .setIcon(R.drawable.ic_navigation_cancel)
                            .setCancelable(false).setNegativeButton(getString(R.string.so_sorry),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            }
                    );
                } else if (msg.equals("2")) {
                    builder.setTitle(getString(R.string.warning))
                            .setMessage(getString(R.string.this_car_in_this_day_limit_overflow))
                            .setIcon(R.drawable.ic_navigation_cancel)
                            .setCancelable(false).setNegativeButton(getString(R.string.so_sorry),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            }
                    );
                } else if (msg.equals("3")) {
                    builder.setTitle(getString(R.string.warning))
                            .setMessage(getString(R.string.message_error_ocured))
                            .setIcon(R.drawable.ic_navigation_cancel)
                            .setCancelable(false).setNegativeButton(getString(R.string.so_sorry),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            }
                    );
                } else {
                    builder.setTitle(getString(R.string.warning))
                            .setMessage(getString(R.string.thanks_for_using_app_record_id_is) + msg)
                            .setIcon(R.drawable.ic_navigation_accept)
                            .setCancelable(false).setNegativeButton(getString(R.string.hooray),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    getZapisiActiveCount();
                                }
                            }
                    );
                }
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

    public void onClickSelectVremya(View view) {
        if (Utils.getPref(getBaseContext()).getString("object_id", "").contentEquals("")) {
            Toast.makeText(getBaseContext(), getString(R.string.choose_carwash_first), Toast.LENGTH_LONG).show();
            return;
        }
        Intent intent = new Intent(this, SelectVremyaActivity.class);
        startActivityForResult(intent, 1);
    }

    public void onClickViewZapis(View view) {
        if (!Utils.getPref(getBaseContext()).contains("hasRecords")) {
            getZapisiActiveCount();
            return;
        }


        if (!Utils.getPref(getBaseContext()).getBoolean("hasRecords", false)) {
            //Ничего
            Toast.makeText(getBaseContext(), getString(R.string.new_booking_records_were_appear_here), Toast.LENGTH_LONG).show();
        }

        //Переход на список записей
        Intent intent = new Intent(this, ZapisListActivity.class);
        startActivityForResult(intent, 1);
    }

    public void getFastChoose() {
        final SharedPreferences pref = Utils.getPref(getBaseContext());
        String car_id = pref.getString("car_id", "");
        String uslugi_id = pref.getString("uslugi_id", "");
        String gorod_id = pref.getString("gorod_id", "");


        RequestParams params = new RequestParams();
        if (uslugi_id.length() > 2) {
            uslugi_id = uslugi_id.substring(1, uslugi_id.length() - 1);
            params.put("uslugi_id", uslugi_id);
        }
        params.add("car_id", car_id);
        params.add("gorod_id", gorod_id);
        params.add("limit", "1");
        params.add("model", "fast_choose");
        params.add("myLat", pref.getString("myLat", ""));
        params.add("myLon", pref.getString("myLon", ""));
        RestClient restClient = new RestClient(this);
        restClient.post("view", params, new RestAsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                super.onSuccess(statusCode, headers, responseBody);
                JSONObject object;
                JSONObject resource;
                try {
                    object = data.getJSONObject("data");
                    object = object.getJSONObject("object");
                    pref.edit().putString("object_id", object.getString("id")).apply();
                    pref.edit().putString("object_name", object.getString("nazvanie")).apply();
                    TextView polzovatel_object = (TextView) findViewById(R.id.polzovatel_object);
                    polzovatel_object.setText(pref.getString("object_name", getString(R.string.not_selected)));

                    resource = data.getJSONObject("data").getJSONObject("resource");
                    resource_id = resource.getString("id");
                    resource_data = resource.getString("data_full").split(" в ")[0];
                    resource_vremya = resource.getString("vremya");
                    resource_vremya = resource_vremya.substring(0, resource_vremya.length() - 3);
                    showSelectedResource();

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void onClickPogoda(View view) {
        Toast.makeText(getBaseContext(), getString(R.string.last_7_days_weather), Toast.LENGTH_LONG).show();
    }
}
