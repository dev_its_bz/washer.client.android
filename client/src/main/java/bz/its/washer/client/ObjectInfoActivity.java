package bz.its.washer.client;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;
import com.loopj.android.image.SmartImageView;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class ObjectInfoActivity extends Activity {

    static String object_id;
    static String object_name;
    static String object_adres;
    static String object_lat;
    static String object_lon;
    static String cena;
    static String dlitelnost;

    static String car_id;
    public String phone = "";
    SectionsPagerAdapter mSectionsPagerAdapter;
    ViewPager mViewPager;
    List<View> pages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.activity_object_info);

        object_id = getIntent().getStringExtra("object_id");
        car_id = getIntent().getStringExtra("car_id");
        dlitelnost = getIntent().getStringExtra("dlitelnost");
        cena = getIntent().getStringExtra("cena");


        pages = new ArrayList<View>();

        LayoutInflater inflater = LayoutInflater.from(this);
        View page = inflater.inflate(R.layout.fragment_object_info_main, null);
        pages.add(page);
        load_object_info_main();

        page = inflater.inflate(R.layout.fragment_object_info_uslugi, null);
        pages.add(page);
        load_object_info_services();

        page = inflater.inflate(R.layout.fragment_object_info_comfort, null);
        pages.add(page);
        load_object_info_comfort();

        page = inflater.inflate(R.layout.fragment_object_info_reviews, null);
        pages.add(page);
        load_object_info_reviews();

        page = inflater.inflate(R.layout.fragment_object_appeal, null);
        pages.add(page);

        mSectionsPagerAdapter = new SectionsPagerAdapter(pages);
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void onClickBack(View v) {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }


    public void onFavoriteClick(View v) {
        if (v.getId() == R.id.imageViewFavorite) {
            ImageView img = (ImageView) v;
            if (img.getTag().toString().equals("inFavorite")) {
                img.setImageResource(R.drawable.ic_rating_favorite_no);
                img.setTag("outFavorite");
                changeFavorite("delete");
                Toast.makeText(this, getString(R.string.removed_from_favorites), Toast.LENGTH_SHORT).show();
            } else {
                img.setImageResource(R.drawable.ic_rating_favorite);
                img.setTag("inFavorite");
                changeFavorite("create");
                Toast.makeText(this, getString(R.string.added_to_favorites), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void changeFavorite(String apiMethod) {
        RequestParams params = new RequestParams();
        params.put("object_id", object_id);
        params.put("model", "favorite");
        RestClient restClient = new RestClient(this);

        restClient.post(apiMethod, params, new RestAsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                super.onSuccess(statusCode, headers, responseBody);
            }
        });
        Intent intent = new Intent();
        intent.putExtra("favoriteChanged", true);
        setResult(RESULT_OK, intent);
    }

    public void onClickCall(View v) {
        Intent dialIntent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone));
        startActivity(dialIntent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void onClickSave(View v) {
        SharedPreferences pref = Utils.getPref(getBaseContext());
        SharedPreferences.Editor prefs = pref.edit();
        String uslugi_id_filter = pref.getString("uslugi_id_filter", "");
        String uslugi_name_filter = pref.getString("uslugi_name_filter", "");
        prefs.putString("uslugi_id", uslugi_id_filter);
        prefs.putString("uslugi_name", uslugi_name_filter);
        prefs.putString("object_id", object_id);
        prefs.putString("object_name", object_name);
        prefs.putString("object_adres", object_adres);
        prefs.putString("cena", cena);
        prefs.putString("dlitelnost", dlitelnost);
        prefs.apply();

        Intent intent = new Intent();
        intent.putExtra("objectSelect", true);
        setResult(RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void onClickObjectAppeal(View v) {
        TextView objectAppeal = (TextView) findViewById(R.id.objectAppealText);
        if (objectAppeal.length() == 0) {
            Toast.makeText(getBaseContext(), getString(R.string.message_cannot_be_empty), Toast.LENGTH_LONG).show();
            return;
        }
        RequestParams params = new RequestParams();
        params.add("appealText", objectAppeal.getText().toString());
        params.add("model", "objectAppeal");
        params.add("object_id", getIntent().getStringExtra("object_id"));
        RestClient restClient = new RestClient(this);
        restClient.post("create", params, new RestAsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                super.onSuccess(statusCode, headers, responseBody);
                Toast.makeText(mActivity, getString(R.string.your_message_are_sended), Toast.LENGTH_SHORT).show();
                mActivity.finish();
            }
        });
    }

    private void load_object_info_comfort() {
        View rootView = pages.get(2);
        AdapterView.OnItemClickListener onComfortClick = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
            }
        };
        RequestParams params = new RequestParams();
        params.put("object_id", object_id);
        RestList comfortList = new RestList(this, rootView, R.id.comfortListView, R.layout.list_item_object_comforts, onComfortClick);
        try {
            comfortList.restLoadList("comfort", params);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void load_object_info_main() {

        final View rootView = pages.get(0);
        SharedPreferences pref = Utils.getPref(getBaseContext());
        String myLat = pref.getString("myLat", "");
        String myLon = pref.getString("myLon", "");

        RequestParams params = new RequestParams();
        params.add("model", "object");
        params.add("object_id", object_id);
        params.add("myLat", myLat);
        params.add("myLon", myLon);

        RestClient restClient = new RestClient(this);
        restClient.post("view", params, new RestAsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                super.onSuccess(statusCode, headers, responseBody);
                try {
                    JSONObject object = data.getJSONArray("data").getJSONObject(0);

                    object_name = object.getString("nazvanie");
                    object_adres = object.getString("adres");
                    object_lat = object.getString("lat");
                    object_lon = object.getString("lon");
                    ((TextView) findViewById(R.id.nazvanie)).setText(object_name);
                    ((TextView) rootView.findViewById(R.id.address)).setText(object_adres);
                    ((RatingBar) rootView.findViewById(R.id.rbObject)).setRating(Integer.parseInt(object.getString("summary")));
                    ((TextView) rootView.findViewById(R.id.summary)).setText(object.getString("rater_count"));
                    ((TextView) rootView.findViewById(R.id.distance)).setText(object.getString("distance"));

                    if (object.getString("telefon").equals("null"))
                        findViewById(R.id.callButton).setVisibility(View.GONE);

                    ((ObjectInfoActivity) mActivity).phone = object.getString("telefon");
                    ImageView img = (ImageView) rootView.findViewById(R.id.imageViewFavorite);
                    if (object.getString("favorite").equals("1")) {
                        img.setImageResource(R.drawable.ic_rating_favorite);
                        img.setTag("inFavorite");
                    } else {
                        img.setImageResource(R.drawable.ic_rating_favorite_no);
                        img.setTag("outFavorite");
                    }
                    SmartImageView map = (SmartImageView) rootView.findViewById(R.id.loaderImageView);
                    int height = map.getHeight();
                    int width = map.getWidth();

                    int imageWidth = 0;
                    int imageHeight = 0;

                    if (width > 450 || height > 450) {
                        if (width > height) {
                            imageHeight = (int) (450.0 * height / width);
                            imageWidth = (int) (450.0);
                        } else {
                            imageWidth = (int) (450.0 * width / height);
                            imageHeight = (int) (450.0);
                        }
                    }

                    String mapUrl = "http://static-maps.yandex.ru/1.x/?l=map&pt=" + object_lon + "," + object_lat + ",pm2lbl&z=14&size=" + imageWidth + "," + imageHeight;
                    map.setImageUrl(mapUrl);
                    map.setScaleType(ImageView.ScaleType.FIT_XY);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void load_object_info_reviews() {
        View rootView = pages.get(3);
        final ListView lv = (ListView) rootView.findViewById(R.id.reviewsListView);
        lv.setEmptyView(rootView.findViewById(android.R.id.empty));
        AdapterView.OnItemClickListener onReviewClick = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
            }
        };

        RequestParams params = new RequestParams();
        params.put("object_id", object_id);
        RestList restList = new RestList(this, rootView, R.id.reviewsListView, R.layout.list_item_reviews, onReviewClick);
        try {
            restList.restLoadList("review", params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void load_object_info_services() {
        View rootView = pages.get(1);
        final ListView lv = (ListView) rootView.findViewById(R.id.uslugi_list);
        lv.setEmptyView(rootView.findViewById(android.R.id.empty));
        AdapterView.OnItemClickListener onServiceClick = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
            }
        };

        RequestParams params = new RequestParams();
        params.put("object_id", object_id);
        params.put("car_id", car_id);
        RestList serviceList = new RestList(this, rootView, R.id.uslugi_list, R.layout.list_item_uslugi, onServiceClick);
        try {
            serviceList.restLoadList("service", params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onClickMap(View view) {
        //Переход на навигационное ПО
        Intent intent;
        intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("geo:" + object_lat + "," + object_lon));
        startActivity(intent);
    }

    public class SectionsPagerAdapter extends PagerAdapterMy {
        public SectionsPagerAdapter(List<View> pages) {
            super(pages);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.object_info_title_section1).toUpperCase(l);
                case 1:
                    return getString(R.string.object_info_title_section2).toUpperCase(l);
                case 2:
                    return getString(R.string.object_info_title_section3).toUpperCase(l);
                case 3:
                    return getString(R.string.object_info_title_section4).toUpperCase(l);
                case 4:
                    return getString(R.string.object_info_title_section5).toUpperCase(l);
            }
            return null;
        }
    }
}
