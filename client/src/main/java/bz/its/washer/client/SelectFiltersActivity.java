package bz.its.washer.client;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class SelectFiltersActivity extends Activity {

    private TextView myChoice;
    private ArrayList<String> sortArray = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.activity_select_filters);

        myChoice = (TextView) findViewById(R.id.filtersMyChoice);
        sortArray = new ArrayList<String>();

        CheckBox check1 = (CheckBox) findViewById(R.id.nearest);
        CheckBox check2 = (CheckBox) findViewById(R.id.early);
        CheckBox check3 = (CheckBox) findViewById(R.id.best_cost);
        CheckBox check4 = (CheckBox) findViewById(R.id.best);
        CheckBox check5 = (CheckBox) findViewById(R.id.like);


        String filters = Utils.getPref(getBaseContext()).getString("filters", "");
        if (!filters.contentEquals("")) {
            ArrayList<String> filters_list = Utils.toArray(filters);
            for (String filter : filters_list) {
                sortArray.add(filter);
                myChoice.append(" > " + filter);
                if (check1.getContentDescription().toString().contentEquals(filter))
                    check1.setChecked(true);
                if (check2.getContentDescription().toString().contentEquals(filter))
                    check2.setChecked(true);
                if (check3.getContentDescription().toString().contentEquals(filter))
                    check3.setChecked(true);
                if (check4.getContentDescription().toString().contentEquals(filter))
                    check4.setChecked(true);
                if (check5.getContentDescription().toString().contentEquals(filter))
                    check5.setChecked(true);
            }
        }
        if (!Utils.getPref(getBaseContext()).getBoolean("LocationOn", false)) {
            findViewById(R.id.nearest).setVisibility(View.GONE);
            sortArray.remove(getString(R.string.filter_nearest_key));
            check1.setChecked(false);
        }

        if (Utils.getPref(getBaseContext()).getString("uslugi_id_filter", "").contentEquals("")) {
            findViewById(R.id.best_cost).setVisibility(View.GONE);
            sortArray.remove(getString(R.string.filter_cheap_key));
            check3.setChecked(false);
        }
    }

    @Override
    public void onBackPressed() {
        finish();//go back to the previous Activity
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void itemClicked(View v) {
        CheckBox checkBox = (CheckBox) v;
        if (checkBox.isChecked()) {
            myChoice.append(" > " + checkBox.getText().toString());
            sortArray.add(checkBox.getContentDescription().toString());
        } else {
            myChoice.setText(myChoice.getText().toString().replaceAll(" > " + checkBox.getText().toString(), ""));
            sortArray.remove(checkBox.getContentDescription().toString());
        }
    }

    public void onClickClear(View v) {
        //TODO: Здесь и выше переделать на обход массива чекбоксов
        CheckBox check1 = (CheckBox) findViewById(R.id.nearest);
        CheckBox check2 = (CheckBox) findViewById(R.id.early);
        CheckBox check3 = (CheckBox) findViewById(R.id.best_cost);
        CheckBox check4 = (CheckBox) findViewById(R.id.best);
        CheckBox check5 = (CheckBox) findViewById(R.id.like);
        check1.setChecked(false);
        check2.setChecked(false);
        check3.setChecked(false);
        check4.setChecked(false);
        check5.setChecked(false);
        sortArray.clear();
        myChoice.setText(getString(R.string.all_carwashes));
        Toast.makeText(getBaseContext(), getString(R.string.filters_are_cleared), Toast.LENGTH_LONG).show();
    }

    public void onClickSave(View v) {
        SharedPreferences pref = Utils.getPref(getBaseContext());
        SharedPreferences.Editor prefs = pref.edit();
        String filters = sortArray.toString();
        if (sortArray.size() == 0) filters = "";
        prefs.putString("filters", filters);
        prefs.apply();

        Intent intent = new Intent();
        intent.putExtra("filtersSelect", true);
        setResult(RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void onClickBack(View v) {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
