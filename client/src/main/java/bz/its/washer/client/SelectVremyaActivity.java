/*
 * Copyright (c) ITS 2014.
 *
 * www.its.bz
 * dev@its.bz
 */

package bz.its.washer.client;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

public class SelectVremyaActivity extends Activity {

    String uslugi_id;
    String dlitelnost;
    String object_id;
    private String checkDate = "0";
    private ArrayList<HashMap<String, String>> resource_data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_vremya);

        resource_data = new ArrayList<HashMap<String, String>>();

        SharedPreferences pref = Utils.getPref(getBaseContext());
        uslugi_id = pref.getString("uslugi_id", "");
        dlitelnost = pref.getString("dlitelnost", "");
        object_id = pref.getString("object_id", "");

        loadResources();

        RadioGroup rg = (RadioGroup) findViewById(R.id.radiogroup);
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.today:
                        checkDate = "0";
                        break;
                    case R.id.tomorrow:
                        checkDate = "1";
                        break;
                }
                loadResources();
            }
        });
    }

    private void loadResources() {
        AdapterView.OnItemClickListener onResourceClick = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                TextView vremya_text = (TextView) view.findViewById(R.id.vremya);
                TextView resource_id_text = (TextView) view.findViewById(R.id.id);
                TextView data_text = (TextView) view.findViewById(R.id.data);
                String resource_vremya = vremya_text.getText().toString();
                String resource_id = resource_id_text.getText().toString();
                String resource_data = data_text.getText().toString();

                Intent intent = new Intent();
                intent.putExtra("resource_id", resource_id);
                intent.putExtra("resource_vremya", resource_vremya);
                intent.putExtra("resource_data", resource_data);
                intent.putExtra("vremyaSelect", true);

                setResult(RESULT_OK, intent);
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
        };

        RequestParams params = new RequestParams();
        params.add("object_id", object_id);
        if (uslugi_id.length() > 2)
            params.add("dlitelnost", "30");
        else {
            params.add("dlitelnost", dlitelnost);
        }
        params.add("date", checkDate);
        params.add("model", "resources");

        RestList resourceList = new RestList(this, null, R.id.resourcesListView, R.layout.list_item_resources, onResourceClick);
        try {
            resourceList.restLoadList("resources", params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        finish();//go back to the previous Activity
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void onClickBack(View v) {
        finish();//go back to the previous Activity
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
