package bz.its.washer.client;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static java.lang.Math.asin;
import static java.lang.Math.cos;
import static java.lang.Math.pow;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;
import static java.lang.Math.toRadians;

/**
 * Created by zolotarev on 21.03.14.
 */

public class Utils {
    public static boolean checkJsonError(Context context, JSONObject js) {
        if (js.has("error")) {
            try {
                JSONObject jsonError = js.getJSONObject("error");
                Toast.makeText(context, jsonError.getString("title") + "\n" + jsonError.getString("text"), Toast.LENGTH_LONG).show();
            } catch (JSONException e) {
                Toast.makeText(context,context.getString(R.string.message_error_ocured), Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
            return true;
        } else
            return false;
    }

    //Возвращает расстояние между точками в километрах
    public static final double getDistance(double lat1, double lon1, double lat2, double lon2) {
        return 2 * asin(sqrt(pow(sin(toRadians((lat1 - lat2) / 2)), 2) +
                cos(toRadians(lat1)) * cos(toRadians(lat2)) *
                        pow(sin(toRadians((lon1 - lon2) / 2)), 2))) * 6378245 / 1000;
    }

    public static String md5(final String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static RequestParams putAuthParams(RequestParams params, Activity activity) {
        SharedPreferences pref = activity.getApplication().getSharedPreferences("bz.its.washer.client", Context.MODE_PRIVATE);

        String auth_polzovatel_id = pref.getString("auth_polzovatel_id", "");
        String auth_polzovatel_hash = pref.getString("auth_polzovatel_hash", "");

        params.add("auth_polzovatel_id", auth_polzovatel_id);
        params.add("auth_polzovatel_hash", auth_polzovatel_hash);
        params.add("gorod_id", getUserGorod(activity));
        return params;
    }

    public static int getId(String resourceName, Class<?> c) {
        try {
            Field idField = c.getDeclaredField(resourceName);
            return idField.getInt(idField);
        } catch (Exception e) {
            throw new RuntimeException("No resource ID found for: "
                    + resourceName + " / " + c, e);
        }
    }

    public static String getUserId(Activity activity) {
        SharedPreferences pref = activity.getApplication().getSharedPreferences("bz.its.washer.client", Context.MODE_PRIVATE);
        return pref.getString("auth_polzovatel_id", "42");
    }

    public static String getUserGorod(Activity activity) {
        SharedPreferences pref = activity.getApplication().getSharedPreferences("bz.its.washer.client", Context.MODE_PRIVATE);
        return pref.getString("gorod_id", "1");
    }

    public static String getUserHash(Activity activity) {
        return md5(getUserId(activity) + getUserPassword(activity));
    }

    public static String getUserPassword(Activity activity) {
        SharedPreferences pref = activity.getApplication().getSharedPreferences("bz.its.washer.client", Context.MODE_PRIVATE);
        return pref.getString("auth_polzovatel_parol", "");
    }

    public static JSONObject getModelView(Activity activity, String model, RequestParams params) {
        JSONObject jsonObj = new JSONParser().getJSONObject(activity, "view", model, params);
        return jsonObj;
    }

    public static void setPersistentObjectSet(Context context, String key, String o) {
        SharedPreferences _store = context.getSharedPreferences("bz.its.washer.client", Context.MODE_PRIVATE);
        synchronized (_store) {
            SharedPreferences.Editor editor = _store.edit();
            if (o == null) {
                editor.remove(key);
            } else {
                Set<String> vals = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    vals = _store.getStringSet(key, null);
                } else {
                    String s = _store.getString(key, null);
                    if (s != null)
                        vals = new HashSet<String>(Arrays.asList(s.split(",")));
                }
                if (vals == null) vals = new HashSet<String>();
                vals.add(o);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    editor.putStringSet(key, vals);
                } else {
                    editor.putString(key, join(vals, ","));
                }
            }
            editor.apply();
        }
    }

    public static void setPersistentObjectSet(Context context, String key, Set o) {
        SharedPreferences _store = context.getSharedPreferences("bz.its.washer.client", Context.MODE_PRIVATE);
        synchronized (_store) {
            SharedPreferences.Editor editor = _store.edit();
            if (o == null) {
                editor.remove(key);
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    editor.putStringSet(key, o);
                } else {
                    editor.putString(key, join(o, ","));
                }
            }
            editor.apply();
        }
    }


    public static Set<String> getPersistentObjectSet(Context context, String key) {
        SharedPreferences _store = context.getSharedPreferences("bz.its.washer.client", Context.MODE_PRIVATE);
        synchronized (_store) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                return _store.getStringSet(key, null);
            } else {
                String s = _store.getString(key, null);
                if (s != null) return new HashSet<String>(Arrays.asList(s.split(",")));
                else return null;
            }
        }
    }


    public static String join(Set<String> set, String delim) {
        StringBuilder sb = new StringBuilder();
        String loopDelim = "";

        if (set != null)
            for (String s : set) {
                sb.append(loopDelim);
                sb.append(s);

                loopDelim = delim;
            }

        return sb.toString();
    }

    public static ArrayList<String> toArray(String s) {
        if (s.length() > 2) {
            s = s.substring(1, s.length() - 1);
            return new ArrayList<String>(Arrays.asList(s.split(", ")));
        } else
            return new ArrayList<String>();
    }

    public static String fromArray(ArrayList<String> s) {
        return s.toString();
    }

    public static SharedPreferences getPref(Context context) {
        return context.getSharedPreferences("bz.its.washer.client", Context.MODE_PRIVATE);
    }
}
