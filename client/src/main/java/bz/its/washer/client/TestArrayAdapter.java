package bz.its.washer.client;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zolotarev on 13.03.14.
 */
public class TestArrayAdapter extends ArrayAdapter {
    Context context;
    List<? extends Map<String, ?>> data;
    private static LayoutInflater inflater = null;
    int item_layout;
    View.OnClickListener ClickOnDel;


    public TestArrayAdapter(Context context, ArrayList<HashMap<String, String>> data, int resource) {
        super(context, resource, data);

        this.context = context;
        this.data = data;
        this.item_layout = resource;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public TestArrayAdapter(Context context, List<? extends Map<String, ?>> data, int resource, View.OnClickListener ClickOnDel) {
        super(context, resource, data);
        this.context = context;
        this.data = data;
        this.item_layout = resource;
        this.ClickOnDel = ClickOnDel;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        //super.getView(position, convertView, parent);

        View vi = convertView;
        HashMap<String, String> itemObject = ((HashMap<String, String>) getItem(position));

        ObjectListHolder holder = null;
        if (vi == null) {
            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = inflater.inflate(item_layout, parent, false);
            holder = new ObjectListHolder();
            holder.objectIdText = (TextView) vi.findViewById(R.id.id);
            holder.nazvanieText = (TextView) vi.findViewById(R.id.nazvanie);
            holder.addressText = (TextView) vi.findViewById(R.id.adres);
            holder.summText = (TextView) vi.findViewById(R.id.summary);
            holder.rb = (RatingBar) vi.findViewById(R.id.rbLike);
            holder.noLike = vi.findViewById(R.id.like);
            holder.distanceText = (TextView) vi.findViewById(R.id.distance);
            holder.latText = (TextView) vi.findViewById(R.id.lat);
            holder.lonText = (TextView) vi.findViewById(R.id.lon);
            holder.costText = (TextView) vi.findViewById(R.id.cena);
            holder.fulltimeText = (TextView) vi.findViewById(R.id.dlitelnost);
            holder.starttimeText = (TextView) vi.findViewById(R.id.nearest_time);

            holder.layout = (LinearLayout) vi.findViewById(R.id.linearLayoutUdobstvo);

            if (item_layout == R.layout.list_item_object_favorites)
                ////     iv = (ImageButton) vi.findViewById(R.id.deleteFromFavorites);
                vi.setTag(holder);
        } else {
            holder = (ObjectListHolder) vi.getTag();
        }


        //  TextView objectIdText = (TextView) vi.findViewById(R.id.id);
        if (holder.objectIdText != null)
            holder.objectIdText.setText(itemObject.get("id").toString());
        if (holder.nazvanieText != null)
            //    TextView nazvanieText = (TextView) vi.findViewById(R.id.nazvanie);
            holder.nazvanieText.setText(itemObject.get("nazvanie").toString());

        //  TextView addressText = (TextView) vi.findViewById(R.id.adres);
        holder.addressText.setText(itemObject.get("adres").toString());

        //   TextView summText = (TextView) vi.findViewById(R.id.summary);
        holder.summText.setText(itemObject.get("rater_count").toString());
        // RatingBar rb = (RatingBar) vi.findViewById(R.id.rbLike);
        if (holder.rb != null)
            if (data.get(position).get("summary").toString().equals("-1")) {
                holder.rb.setVisibility(RatingBar.GONE);
                holder.noLike.setVisibility(View.VISIBLE);
            } else holder.rb.setRating(Integer.parseInt(itemObject.get("summary").toString()));

        //   TextView distanceText = (TextView) vi.findViewById(R.id.distance);
        holder.distanceText.setText(itemObject.get("distance").toString() + "км");

        // TextView latText = (TextView) vi.findViewById(R.id.lat);
        holder.latText.setText(itemObject.get("lat").toString());

        //   TextView lonText = (TextView) vi.findViewById(R.id.lon);
        holder.lonText.setText(itemObject.get("lon").toString());
        if (holder.costText != null)
            //  TextView cenaText = (TextView) vi.findViewById(R.id.cost);
            if (itemObject.get("full_cost") != null) {
                holder.costText.setText(itemObject.get("full_cost").toString());
            } else
                holder.costText.setVisibility(TextView.GONE);
        if (holder.fulltimeText != null)
            //  TextView dlitelnostText = (TextView) vi.findViewById(R.id.full_time);
            if (itemObject.get("full_time") != null) {
                holder.fulltimeText.setText(itemObject.get("full_time").toString());
            } else
                holder.fulltimeText.setVisibility(TextView.GONE);

        //  TextView nearestTimeText = (TextView) vi.findViewById(R.id.start_time);
        if (itemObject.get("start_vremya").toString().equals("null")) {
            holder.starttimeText.setText("неизвестно");
        } else
            holder.starttimeText.setText(itemObject.get("start_vremya").toString());
        if (holder.layout != null)
            if (!itemObject.get("udobstvo").equals("[]"))
                try {
                    //  LinearLayout layout = (LinearLayout) vi.findViewById(R.id.linearLayoutUdobstvo);
                    holder.layout.removeAllViews();

                    JSONObject jsonObject = new JSONObject(itemObject.get("udobstvo").toString());
                    JSONArray ar = jsonObject.names();

                    for (int i = 0; i < ar.length(); i++) {
                        String s = ar.get(i).toString() + "_small";
                        addImg(holder.layout, getId(s, R.drawable.class));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


        if (item_layout == R.layout.list_item_object_favorites) {
            //       iv.setOnClickListener(ClickOnDel);
            //     iv.setTag(position);
        }


        return vi;
    }

    //TODO писать holder - класс см. закладки // nonono проблема в том что position меняется при скроле // переписать все на Array Adapter (вроде будет норм)
    static class ObjectListHolder {
        TextView objectIdText;
        TextView nazvanieText;

        TextView addressText;
        View noLike;
        TextView summText;

        RatingBar rb;
        TextView distanceText;
        TextView latText;
        TextView lonText;
        TextView costText;
        TextView fulltimeText;
        TextView starttimeText;
        LinearLayout layout;
        ImageButton iv;

    }


    private void addImg(LinearLayout layout, int id) {
        ImageView img = new ImageView(context);
        img.setImageResource(id);
        layout.addView(img);

    }

    public static int getId(String resourceName, Class<?> c) {
        try {
            Field idField = c.getDeclaredField(resourceName);
            return idField.getInt(idField);
        } catch (Exception e) {
            throw new RuntimeException("No resource ID found for: "
                    + resourceName + " / " + c, e);
        }
    }

}
