package bz.its.washer.client;

/**
 * Created by Пользователь on 14.05.2014.
 */

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;

import java.io.IOException;


public class RegisterApp extends AsyncTask<Void, Void, String> {

    private static final String TAG = "GCMRelated";
    private static Activity mActivity;
    Context ctx;
    GoogleCloudMessaging gcm;
    String SENDER_ID = "19174205492";
    String regid = null;

    public RegisterApp(Context ctx, GoogleCloudMessaging gcm, Activity activity) {
        this.ctx = ctx;
        this.gcm = gcm;
        mActivity = activity;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        if (!result.contentEquals(""))
            sendRegistrationIdToBackend();
    }


    @Override
    protected String doInBackground(Void... arg0) {
        regid = "";
        try {
            if (gcm == null) {
                gcm = GoogleCloudMessaging.getInstance(ctx);
            }
            regid = gcm.register(SENDER_ID);
            Utils.getPref(ctx).edit().putString("registration_id", regid).apply();
        } catch (IOException ex) {
            Log.v("bz.its.washer.client", ex.getMessage());
        }
        return regid;
    }

    private void sendRegistrationIdToBackend() {
        RequestParams params = new RequestParams();
        params.put("model", "gcm");
        params.put("gcm_id", regid);
        RestClient restClient = new RestClient(mActivity, false);
        restClient.post("create", params, new RestAsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                super.onSuccess(statusCode, headers, responseBody);
                Log.v("bz.its.washer.client", "GCM SAVED");
            }
        });
    }
}

