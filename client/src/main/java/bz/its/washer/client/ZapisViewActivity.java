/*
 * Copyright (c) ITS 2014.
 *
 * www.its.bz
 * dev@its.bz
 */

package bz.its.washer.client;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

public class ZapisViewActivity extends Activity {

    String zapis_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.activity_zapis_view);

        zapis_id = getIntent().getStringExtra("zapis_id");
        RequestParams params = new RequestParams();
        params.add("model", "zapis_info");
        params.add("zapis_id", zapis_id);

        RestClient restClient = new RestClient(this, true);
        restClient.post("view", params, new RestAsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        super.onSuccess(statusCode, headers, responseBody);
                        try {
                            JSONObject object = data.getJSONArray("data").getJSONObject(0);
                            ((TextView) findViewById(R.id.nazvanie)).setText(getString(R.string.record_no) + zapis_id);
                            ((TextView) findViewById(R.id.polzovatel_car)).setText(object.getString("car_name"));
                            ((TextView) findViewById(R.id.polzovatel_car_gruppa)).setText(object.getString("car_gruppa_name"));
                            ((TextView) findViewById(R.id.polzovatel_object)).setText(object.getString("object_name"));
                            ((TextView) findViewById(R.id.polzovatel_object_adres)).setText(object.getString("adres"));
                            ((TextView) findViewById(R.id.polzovatel_vremya)).setText(object.getString("record_time_full"));

                            String cena = object.getString("cena");
                            if (!cena.contentEquals("---")) {
                                String dlitelnost = object.getString("dlitelnost");
                                ((TextView) findViewById(R.id.cena)).setText(cena);
                                ((TextView) findViewById(R.id.dlitelnost)).setText(dlitelnost);
                                findViewById(R.id.uslugi_empty).setVisibility(View.GONE);
                                findViewById(R.id.uslugi_detali).setVisibility(View.VISIBLE);
                            } else {
                                findViewById(R.id.uslugi_empty).setVisibility(View.VISIBLE);
                                findViewById(R.id.uslugi_detali).setVisibility(View.GONE);
                            }

                            RequestParams params = new RequestParams();
                            params.add("zapis_id", zapis_id);
                            RestList carList = new RestList(ZapisViewActivity.this, null, R.id.uslugi_list, R.layout.list_item_uslugi, null);
                            try {
                                carList.restLoadList("zapis_usluga", params);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            finish();
                        }
                    }
                }
        );
    }

    public void onClickDelete(View view) {
        //Удаление записи (ставим статус = 4)
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        RequestParams params = new RequestParams();
                        params.add("model", "zapis");
                        params.put("zapis_id", zapis_id);
                        RestClient restClient = new RestClient(ZapisViewActivity.this, false);
                        restClient.post("delete", params, new RestAsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                Intent intent = new Intent();
                                intent.putExtra("zapisDelete", true);
                                setResult(RESULT_OK, intent);
                                Toast.makeText(getBaseContext(), getString(R.string.record_has_been_deleted_dispatcher_will_be_notified), Toast.LENGTH_LONG).show();
                                finish();
                                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                            }
                        });
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.delete_record)).setPositiveButton(getString(R.string.yes), dialogClickListener)
                .setNegativeButton(getString(R.string.no), dialogClickListener).show();
    }

    public void onClickObject(View view) {
        //TODO: Просмотр объекта
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void onClickBack(View v) {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

}
