package bz.its.washer.client;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by zolotarev on 19.03.14.
 */
public class LoaderJSONList extends AsyncTask<String, String, JSONArray> {

    public String model = "";
    public String action = "list";
    public RequestParams params;
    public int listViewResourceId;
    public int listViewItemResourceId;
    public Activity activity;
    public View view;
    public AdapterView.OnItemClickListener onClick;

    private ProgressDialog pDialog;
    private ListView listView;
    private ArrayList<HashMap<String, String>> JSONList = new ArrayList<HashMap<String, String>>();


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pDialog = new ProgressDialog(activity);
        pDialog.setMessage(activity.getString(R.string.loading_message));
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
    }

    @Override
    protected JSONArray doInBackground(String... args) {
        JSONParser jParser = new JSONParser();
        JSONArray json = jParser.getJSONArray(activity, action, model, params);
        return json;
    }

    @Override
    protected void onPostExecute(JSONArray json) {
        pDialog.dismiss();
        if (json == null)
            json = new JSONArray();
        for (int i = 0; i < json.length(); i++) {
            JSONObject c = null;
            try {
                c = json.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            HashMap<String, String> map = new HashMap<String, String>();
            Iterator keys = c.keys();
            while (keys.hasNext()) {
                String key = (String) keys.next();
                try {
                    map.put(key, c.getString(key));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            JSONList.add(map);
        }

        List<String> listKeys = new ArrayList<String>();
        List<Integer> listRes = new ArrayList<Integer>();

        if (json.length() > 0) {
            JSONObject c = null;
            try {
                c = json.getJSONObject(0);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            int i = 0;
            Iterator keys = c.keys();
            while (keys.hasNext()) {
                String key = (String) keys.next();
                int resID = activity.getResources().getIdentifier(key, "id", "bz.its.washer.client");

                if (resID != 0) {
                    listKeys.add(key);
                    listRes.add(resID);
                    i++;
                }
            }

            String[] arrKeys = new String[listKeys.size()];
            listKeys.toArray(arrKeys);

            int[] arrRes = new int[listRes.size()];
            for (int k = 0; k < listRes.size(); k++)
                arrRes[k] = listRes.get(k);

            listView = (ListView) view.findViewById(listViewResourceId);
            ListAdapter adapter = new SimpleAdapter(activity, JSONList,
                    listViewItemResourceId, arrKeys, arrRes);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(onClick);
        }
    }
}
