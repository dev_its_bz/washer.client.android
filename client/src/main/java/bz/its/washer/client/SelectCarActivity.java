/*
 * Copyright (c) ITS 2014.
 *
 * www.its.bz
 * dev@its.bz
 */

package bz.its.washer.client;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import org.json.JSONException;

public class SelectCarActivity extends Activity {
    private boolean cars_changed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.activity_select_car);
        loadCarList();
    }

    private void loadCarList() {
        AdapterView.OnItemClickListener onCarClick;
        onCarClick = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                TextView carIdText = (TextView) view.findViewById(R.id.id);
                String carId = carIdText.getText().toString();
                String carName = ((TextView) view.findViewById(R.id.car_marka)).getText().toString() + ' ' +
                        ((TextView) view.findViewById(R.id.car_model)).getText().toString();
                String carNameNomer = ((TextView) view.findViewById(R.id.gos_nomer)).getText().toString();

                SharedPreferences pref = Utils.getPref(getBaseContext());
                SharedPreferences.Editor prefs = pref.edit();
                prefs.putString("car_id", carId);
                prefs.putString("car_name", carName);
                prefs.putString("car_name_nomer", carNameNomer);
                prefs.apply();

                Intent intent = new Intent();
                intent.putExtra("carSelect", true);
                setResult(RESULT_OK, intent);
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
        };

        RequestParams params = new RequestParams();
        RestList carList = new RestList(this, null, R.id.carListView, R.layout.list_item_polzovatel_cars, onCarClick);
        try {
            carList.restLoadList("polzovatel_car", params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void onClickCarAdd(View v) {
        Intent addOrEditCar = new Intent(this, CarEditActivity.class);
        addOrEditCar.putExtra("isNew", true);
        startActivityForResult(addOrEditCar, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            return;
        }

        if (data.hasExtra("carSave")) {
            cars_changed = true;
            loadCarList();
        }
    }


    public void onClickEditCar(View v) {
        Intent addOrEditCar = new Intent(this, CarEditActivity.class);

        LinearLayout l = (LinearLayout) v.getParent();

        TextView carIdText = (TextView) l.findViewById(R.id.id);
        String carId = carIdText.getText().toString();
        addOrEditCar.putExtra("car_id", carId);
        addOrEditCar.putExtra("isNew", false);
        startActivityForResult(addOrEditCar, 1);
    }


    @Override
    public void onBackPressed() {
        if (cars_changed) {
            Intent intent = new Intent();
            intent.putExtra("carSelect", true);
            setResult(RESULT_OK, intent);
        }
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void onClickBack(View v) {
        onBackPressed();
    }
}
