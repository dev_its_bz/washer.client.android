/*
 * Copyright (c) ITS 2014.
 *
 * www.its.bz
 * dev@its.bz
 */

package bz.its.washer.client;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.bugsense.trace.BugSenseHandler;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

/**
 * Created by zolotarev on 21.03.14.
 */
public class RestAsyncHttpResponseHandler extends AsyncHttpResponseHandler {
    public Activity mActivity;
    public Context mContext;
    public AsyncHttpClient mClient;
    public JSONObject data = null;
    public boolean mKillActivityOnFailure = true;
    private ProgressDialog pDialog;

    @Override
    public void onStart() {
        //mActivity.setProgressBarVisibility(true);
        //mActivity.setProgress(0);

        pDialog = new ProgressDialog(mActivity);
        pDialog.setMessage("Загрузка..");
        pDialog.setIndeterminate(true);
        pDialog.setProgressDrawable(mActivity.getResources().getDrawable(R.drawable.apptheme_progress_indeterminate_horizontal_holo_light));
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                mClient.cancelRequests(mActivity.getBaseContext(), true);
            }
        });
        pDialog.show();
    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
        //Выкачиваем строку вида {"data":[ТУТ МАССИВ]} и пробуем получить JSON
        Log.v("bz.its.washer.client", new String(responseBody));
        try {
            data = new JSONObject(new String(responseBody, "utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
        if (responseBody != null)
            Log.v("bz.its.washer.client", new String(responseBody));
        else
            Log.v("bz.its.washer.client", "responseBody ===> NULL");

        String body = "";

        if (responseBody != null) {
            if (responseBody.length > 0) {

                try {
                    body = new String(responseBody, "utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, mActivity.getString(R.string.encoding_error) + e.getMessage(), Toast.LENGTH_LONG).show();
                    return;
                }
                try {
                    data = new JSONObject(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, mActivity.getString(R.string.validation_error) + e.getMessage(), Toast.LENGTH_LONG).show();
                    return;
                }
                try {
                    data = data.getJSONObject("data");
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, mActivity.getString(R.string.unknown_error) + e.getMessage(), Toast.LENGTH_LONG).show();
                    return;
                }
                if (data.has("error")) {
                    try {
                        data = data.getJSONObject("error");
                        if (!data.getString("errCode").equals("")) {
                            SharedPreferences pref = mActivity.getApplication().getSharedPreferences("bz.its.washer.client", Context.MODE_PRIVATE);
                            SharedPreferences.Editor prefs = pref.edit();
                            prefs.clear();
                            prefs.apply();
                            Intent loginActivity = new Intent(mActivity.getBaseContext(), LoginActivity.class);
                            mActivity.finish();
                            mActivity.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                            mActivity.startActivity(loginActivity);
                        } else {
                            try {
                                Toast.makeText(mContext, data.getString("title") + "\n" + data.getString("text"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(mContext, mActivity.getString(R.string.error_of_handling_error_ocured) + body, Toast.LENGTH_LONG).show();
                        BugSenseHandler.sendException(e);
                        return;
                    }
                } else {
                    Toast.makeText(mContext, mActivity.getString(R.string.strange_error_ocured) + body, Toast.LENGTH_LONG).show();
                }
            } else
                Toast.makeText(mContext, mActivity.getString(R.string.server_not_response), Toast.LENGTH_LONG).show();
        } else
            Toast.makeText(mContext, mActivity.getString(R.string.error_connecting_to_server), Toast.LENGTH_LONG).show();

        if (mKillActivityOnFailure) {
            mActivity.finish();
            mActivity.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }
    }

    @Override
    public void onCancel() {
        /*
        pDialog.dismiss();
        mClient.cancelRequests(mContext, true);
        mClient.getHttpClient().getConnectionManager().shutdown();
        mClient.setMaxRetriesAndTimeout(0,0);
        mActivity.finish();
        //TODO: Не могу культурно остановить попытки переподключения
        */
    }

    @Override
    public void onRetry(int retryNo) {
        pDialog.hide();
        pDialog.setMessage(mActivity.getString(R.string.retry_number) + String.valueOf(retryNo) + "..");
        pDialog.show();
    }

    @Override
    public void onProgress(int bytesWritten, int totalSize) {
        super.onProgress(bytesWritten, totalSize);
        pDialog.setMax(totalSize);
        pDialog.setProgress(bytesWritten);
        //mActivity.setProgress(bytesWritten/totalSize*100);
    }

    @Override
    public void onFinish() {
        super.onFinish();
        try {
            pDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
            BugSenseHandler.sendException(e);
        }
    }
}
