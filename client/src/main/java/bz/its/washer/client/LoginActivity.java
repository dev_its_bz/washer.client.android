/*
 * Copyright (c) ITS 2014.
 *
 * www.its.bz
 * dev@its.bz
 */

package bz.its.washer.client;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import static bz.its.washer.client.Utils.checkJsonError;
import static bz.its.washer.client.Utils.md5;


public class LoginActivity extends Activity {

    private EditText profileName;
    private EditText profileEmail;
    private EditText userPassword;
    private EditText profilePhone;
    private EditText verifyCode;

    private Button loginButton;
    private LinearLayout hintPhoneView;
    private String verifyCodeValue = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        Intent slides = new Intent(this, SlidesActivity.class);
        startActivity(slides);
        setContentView(R.layout.activity_login);

        loginButton = (Button) findViewById(R.id.loginButton);
        profileName = (EditText) findViewById(R.id.profileName);
        profileEmail = (EditText) findViewById(R.id.profileEmail);
        verifyCode = (EditText) findViewById(R.id.verifyCode);
        final TextView loginHintView = (TextView) findViewById(R.id.loginHintView);
        profilePhone = (EditText) findViewById(R.id.user_login);
        profilePhone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus) {
                    loginHintView.setVisibility(View.VISIBLE);
                    loginHintView.setText(getString(R.string.login_form_hint_login));
                }
            }
        });

        userPassword = (EditText) findViewById(R.id.user_password);
        userPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus) {
                    loginHintView.setVisibility(View.VISIBLE);
                    loginHintView.setText(getString(R.string.login_form_hint));
                }
            }
        });

        profilePhone.addTextChangedListener(new MaskedWatcher(getString(R.string.phone_mask)));
        hintPhoneView = (LinearLayout) findViewById(R.id.hintPhoneView);


        verifyCode.setVisibility(View.GONE);
        profileName.setVisibility(View.GONE);
        profileEmail.setVisibility(View.GONE);
        hintPhoneView.setVisibility(View.VISIBLE);

        verifyCode.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                final EditText verifyCode = (EditText) findViewById(R.id.verifyCode);
                if (md5(verifyCode.getText().toString()).contentEquals(verifyCodeValue)) {
                    loginUser(profilePhone.getText().toString(), userPassword.getText().toString());
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void loginUser(String phone, String password) {
        RequestParams params = new RequestParams();
        params.put("telefon", phone);
        params.put("password", password);
        params.put("verifyCode", verifyCode.getText().toString());
        params.put("profileName", profileName.getText().toString());
        params.put("profileEmail", profileEmail.getText().toString());
        RestClient restClient = new RestClient(this, false);
        restClient.post("login", params, new RestAsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                super.onSuccess(statusCode, headers, responseBody);
                try {
                    JSONObject profile = data.getJSONObject("data");

                    if (checkJsonError(getApplicationContext(), profile)) {
                        doRepairPassword();
                        return;
                    }

                    String userID = "", userPasswd = "";
                    try {
                        profileName.setText(profile.getString("fio"));
                        profileEmail.setText(profile.getString("email"));
                        userID = profile.getString("id");
                        userPasswd = profile.getString("parol");
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(LoginActivity.this, getString(R.string.message_error_ocured) + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    setSharedPref(userID, userPasswd);

                    finish();
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                    Intent main = new Intent(getApplicationContext(), MainActivity.class);
                    main.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(main);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), getString(R.string.message_error_ocured) + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            private void setSharedPref(String userID, String userPasswd) {
                SharedPreferences pref = Utils.getPref(getBaseContext());
                SharedPreferences.Editor prefs = pref.edit();
                prefs.putString("auth_polzovatel_id", userID);
                prefs.putString("auth_polzovatel_parol", userPasswd);
                prefs.putString("auth_polzovatel_hash", md5(userID + userPasswd));
                prefs.apply();

                RequestParams params = new RequestParams();
                params.put("model", "polzovatel_nastroiki");
                RestClient restClient = new RestClient(LoginActivity.this, false);
                restClient.post("view", params, new RestAsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        super.onSuccess(statusCode, headers, responseBody);
                        try {
                            JSONObject settings = data.getJSONObject("data");

                            SharedPreferences prefs1 = PreferenceManager.getDefaultSharedPreferences(getApplication());
                            SharedPreferences.Editor ed = prefs1.edit();
                            ed.putBoolean("service_enable", settings.getInt("service_enable") == 1);
                            ed.putString("objects_limit", String.valueOf(settings.getInt("objects_limit")));
                            ed.apply();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), getString(R.string.message_error_ocured) + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();//go back to the previous Activity
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }


    public void onClickForgotPassword() {
        if (profilePhone.getText().toString().length() < 13) {
            Toast.makeText(getApplicationContext(), getString(R.string.phone_number_is_needed_for_password_restore), Toast.LENGTH_LONG).show();
            return;
        }

        RequestParams Userparams = new RequestParams();
        Userparams.put("telefon", profilePhone.getText().toString());
        RestClient restClient = new RestClient(this, false);
        restClient.post("userStatus", Userparams, new RestAsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                super.onSuccess(statusCode, headers, responseBody);
                Boolean newUser = true;
                try {
                    newUser = data.getJSONObject("data").getBoolean("isNew");
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), getString(R.string.message_error_ocured) + e.getMessage(), Toast.LENGTH_LONG).show();
                }

                if (!newUser) {
                    RequestParams params = new RequestParams();
                    params.put("telefon", profilePhone.getText().toString());
                    RestClient restClient = new RestClient(LoginActivity.this, false);
                    restClient.post("verify", params, new RestAsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            super.onSuccess(statusCode, headers, responseBody);
                            try {
                                verifyCodeValue = data.getJSONObject("data").getString("verifyCode");
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), getString(R.string.message_error_ocured) + e.getMessage(), Toast.LENGTH_LONG).show();
                            }
                            Intent changePassword = new Intent(getApplicationContext(), ChangePasswordActivity.class);
                            changePassword.putExtra("phone", profilePhone.getText().toString());
                            changePassword.putExtra("verifyCodeValue", verifyCodeValue);
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                            startActivityForResult(changePassword, 1);
                        }
                    });
                } else
                    Toast.makeText(getApplicationContext(), getString(R.string.no_user_with_this_phone_number_found), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void onClickLogin(View v) {
        if (profilePhone.getText().toString().length() < 13) {
            Toast.makeText(getApplicationContext(), getString(R.string.phone_filled_not_correctly), Toast.LENGTH_LONG).show();
            return;
        }

        if (userPassword.getText().toString().length() < 5) {
            Toast.makeText(getApplicationContext(), getString(R.string.password_length_must_be), Toast.LENGTH_LONG).show();
            return;
        }

        RequestParams UserParams = new RequestParams();
        UserParams.put("telefon", profilePhone.getText().toString());
        UserParams.put("password", userPassword.getText().toString());
        RestClient restClient = new RestClient(this, false);
        restClient.post("userStatus", UserParams, new RestAsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                super.onSuccess(statusCode, headers, responseBody);
                Boolean newUser = true;
                try {
                    newUser = data.getJSONObject("data").getBoolean("isNew");
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), getString(R.string.message_error_ocured) + e.getMessage(), Toast.LENGTH_LONG).show();
                }

                if (newUser) {
                    profileName.setVisibility(View.VISIBLE);
                    profileEmail.setVisibility(View.VISIBLE);
                    verifyCode.setVisibility(View.VISIBLE);
                    profilePhone.setVisibility(View.GONE);
                    userPassword.setVisibility(View.GONE);
                    loginButton.setVisibility(View.GONE);
                    getVerifyCode(profilePhone.getText().toString());
                } else
                    loginUser(profilePhone.getText().toString(), userPassword.getText().toString());
            }
        });
    }

    protected void doRepairPassword() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        onClickForgotPassword();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.access_denied));
        builder.setMessage(getString(R.string.try_to_restore_pasword)).setPositiveButton(getString(R.string.yes), dialogClickListener)
                .setNegativeButton(getString(R.string.no), dialogClickListener).show();
    }

    protected void getVerifyCode(String phone) {
        RequestParams params = new RequestParams();
        params.put("telefon", phone);
        RestClient restClient = new RestClient(this, false);
        restClient.post("verify", params, new RestAsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                super.onSuccess(statusCode, headers, responseBody);
                try {
                    LoginActivity.this.verifyCodeValue = data.getJSONObject("data").getString("verifyCode");
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), getString(R.string.message_error_ocured) + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            return;
        }

        if (data.hasExtra("passwordChanged")) {
            userPassword.setText(data.getStringExtra("newPassword"));
            onClickLogin(loginButton);
        }
    }
}
