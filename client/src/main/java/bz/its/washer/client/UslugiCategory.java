package bz.its.washer.client;

import java.util.ArrayList;

/**
 * Created by Пользователь on 29.04.2014.
 */
public class UslugiCategory {
    public String category_name = null;
    public ArrayList<SubCategory> subcategory_array = new ArrayList<SubCategory>();

    public void ClearAll(){
        for (SubCategory item:subcategory_array)
            item.selected=false;
    }
}
