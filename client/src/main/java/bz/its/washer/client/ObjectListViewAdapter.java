package bz.its.washer.client;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.loopj.android.image.SmartImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

/**
 * Created by zolotarev on 13.03.14.
 */
public class ObjectListViewAdapter extends SimpleAdapter {
    private static LayoutInflater inflater = null;
    Context context;
    List<? extends Map<String, ?>> data;
    int item_layout;
    View.OnClickListener ClickOnDel;

    /**
     * Constructor
     *
     * @param context  The context where the View associated with this SimpleAdapter is running
     * @param data     A List of Maps. Each entry in the List corresponds to one row in the list. The
     *                 Maps contain the data for each row, and should include all the entries specified in
     *                 "from"
     * @param resource Resource identifier of a view layout that defines the views for this list
     *                 item. The layout file should include at least those named views defined in "to"
     * @param from     A list of column names that will be added to the Map associated with each
     *                 item.
     * @param to       The views that should display column in the "from" parameter. These should all be
     *                 TextViews. The first N views in this list are given the values of the first N columns
     */
    public ObjectListViewAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {
        super(context, data, resource, from, to);
        this.context = context;
        this.data = data;
        this.item_layout = resource;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public ObjectListViewAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to, View.OnClickListener ClickOnDel) {
        super(context, data, resource, from, to);
        this.context = context;
        this.data = data;
        this.item_layout = resource;
        this.ClickOnDel = ClickOnDel;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

/*
    public ObjectListViewAdapter(Context context, String[] data) {
        // TODO Auto-generated constructor stub
        this.context = context;
        this.data = data;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
}
*/

    public static int getId(String resourceName, Class<?> c) {
        try {
            Field idField = c.getDeclaredField(resourceName);
            return idField.getInt(idField);
        } catch (Exception e) {
            throw new RuntimeException("No resource ID found for: "
                    + resourceName + " / " + c, e);
        }
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        //super.getView(position, convertView, parent);

        View vi = convertView;
        ObjectListHolder holder = null;
        if (vi == null) {
            vi = inflater.inflate(item_layout, parent, false);
            holder = new ObjectListHolder();
            holder.objectIdText = (TextView) vi.findViewById(R.id.id);
            holder.nazvanieText = (TextView) vi.findViewById(R.id.nazvanie);
            holder.addressText = (TextView) vi.findViewById(R.id.adres);
            holder.summText = (TextView) vi.findViewById(R.id.summary);
            holder.rb = (RatingBar) vi.findViewById(R.id.rbLike);
            holder.distanceText = (TextView) vi.findViewById(R.id.distance);
            holder.latText = (TextView) vi.findViewById(R.id.lat);
            holder.lonText = (TextView) vi.findViewById(R.id.lon);
            holder.cenaText = (TextView) vi.findViewById(R.id.cena);
            holder.dlitelnostText = (TextView) vi.findViewById(R.id.dlitelnost);
            holder.nearestTimeText = (TextView) vi.findViewById(R.id.nearest_time);
            holder.image = (SmartImageView) vi.findViewById(R.id.image);
            holder.layout = (LinearLayout) vi.findViewById(R.id.linearLayoutUdobstvo);
            if (item_layout == R.layout.list_item_object_favorites)
                holder.iv = (ImageView) vi.findViewById(R.id.deleteFromFavorites);
            else
                holder.iv = (ImageView) vi.findViewById(R.id.favorite);
            vi.setTag(holder);
        } else {
            holder = (ObjectListHolder) vi.getTag();
        }


        //  TextView objectIdText = (TextView) vi.findViewById(R.id.id);
        holder.objectIdText.setText(data.get(position).get("id").toString());

        //    TextView nazvanieText = (TextView) vi.findViewById(R.id.nazvanie);
        holder.nazvanieText.setText(data.get(position).get("nazvanie").toString());

        //  TextView addressText = (TextView) vi.findViewById(R.id.adres);
        if (!data.get(position).get("adres").toString().contentEquals("null")) {
            holder.addressText.setText(data.get(position).get("adres").toString());
            holder.addressText.setVisibility(View.VISIBLE);
        } else
            holder.addressText.setVisibility(View.GONE);

        //   TextView summText = (TextView) vi.findViewById(R.id.summary);
        holder.summText.setText(data.get(position).get("rater_count").toString());

        // RatingBar rb = (RatingBar) vi.findViewById(R.id.rbLike);
        if (data.get(position).get("summary").toString().equals("-1")) {
            holder.rb.setRating(0);
            //holder.rb.setVisibility(RatingBar.GONE);
        } else {
            holder.rb.setRating(Integer.parseInt(data.get(position).get("summary").toString()));
            //holder.rb.setVisibility(RatingBar.VISIBLE);
        }

        //   TextView distanceText = (TextView) vi.findViewById(R.id.distance);
        if (Utils.getPref(context).getBoolean("LocationOn", false)) {
            holder.distanceText.setText(data.get(position).get("distance").toString() + context.getString(R.string.distance_km));
            holder.distanceText.setVisibility(View.VISIBLE);
        } else
            holder.distanceText.setVisibility(View.GONE);

        // TextView latText = (TextView) vi.findViewById(R.id.lat);
        holder.latText.setText(data.get(position).get("lat").toString());

        //   TextView lonText = (TextView) vi.findViewById(R.id.lon);
        holder.lonText.setText(data.get(position).get("lon").toString());

        if (data.get(position).get("favorite").toString().contentEquals("1")) {
            holder.iv.setImageResource(R.drawable.ic_rating_favorite);
        } else {
            holder.iv.setImageResource(R.drawable.ic_rating_favorite_no);
        }

        //  TextView cenaText = (TextView) vi.findViewById(R.id.cost);
        if (data.get(position).get("full_cost") != null) {
            holder.cenaText.setVisibility(TextView.VISIBLE);
            holder.cenaText.setText(data.get(position).get("full_cost").toString());
        } else
            holder.cenaText.setVisibility(TextView.GONE);

        //  TextView dlitelnostText = (TextView) vi.findViewById(R.id.full_time);
        if (data.get(position).get("full_time") != null) {
            holder.dlitelnostText.setText(data.get(position).get("full_time").toString());
            holder.dlitelnostText.setVisibility(TextView.VISIBLE);
        } else
            holder.dlitelnostText.setVisibility(TextView.GONE);

        //  TextView nearestTimeText = (TextView) vi.findViewById(R.id.start_time);
        if (data.get(position).get("start_vremya").toString().equals("null")) {
            holder.nearestTimeText.setText(context.getString(R.string.no_data));
        } else
            holder.nearestTimeText.setText(data.get(position).get("start_vremya").toString());

        if (!data.get(position).get("start_vremya").toString().equals("null")) {
            holder.image.setVisibility(View.VISIBLE);
            holder.image.setImageUrl(context.getString(R.string.baseSiteUrl) + "images/" + data.get(position).get("image").toString() + "?" + String.valueOf(System.currentTimeMillis()));
        } else
            holder.image.setVisibility(View.GONE);

        if (!data.get(position).get("udobstvo").equals("[]")) {
            try {
                //  LinearLayout layout = (LinearLayout) vi.findViewById(R.id.linearLayoutUdobstvo);
                holder.layout.removeAllViews();

                JSONObject jsonObject = new JSONObject(data.get(position).get("udobstvo").toString());
                JSONArray ar = jsonObject.names();

                for (int i = 0; i < ar.length(); i++) {
                    String s = ar.get(i).toString() + "_small";
                    addImg(holder.layout, getId(s, R.drawable.class));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else holder.layout.removeAllViews();

        if (item_layout == R.layout.list_item_object_favorites) {
            holder.iv.setOnClickListener(ClickOnDel);
            holder.iv.setTag(position);
        } else {
            //holder.iv.setOnClickListener(ClickOnList);
            holder.iv.setTag(position);
        }

        return vi;
    }

    private void addImg(LinearLayout layout, int id) {
        ImageView img = new ImageView(context);
        img.setImageResource(id);
        layout.addView(img);

    }

    //TODO писать holder - класс см. закладки // nonono проблема в том что position меняется при скроле // переписать все на Array Adapter (вроде будет норм)
    static class ObjectListHolder {
        TextView objectIdText;
        TextView nazvanieText;

        TextView addressText;
        TextView summText;

        RatingBar rb;
        TextView distanceText;
        TextView latText;
        TextView lonText;
        TextView cenaText;
        TextView dlitelnostText;
        TextView nearestTimeText;
        SmartImageView image;
        LinearLayout layout;
        ImageView iv;
    }
}
