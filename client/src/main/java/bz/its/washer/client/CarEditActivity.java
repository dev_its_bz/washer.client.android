/*
 * Copyright (c) ITS 2014.
 *
 * www.its.bz
 * dev@its.bz
 */

package bz.its.washer.client;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;


public class CarEditActivity extends Activity {

    private boolean isNew;
    private String carModelId = "";
    private String carId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.activity_car_edit);

        isNew = getIntent().getBooleanExtra("isNew", true);

        if (!isNew) {
            carId = getIntent().getStringExtra("car_id");

            RequestParams params = new RequestParams();
            params.add("car_id", carId);
            params.add("model", "polzovatel_car");
            RestClient restClient = new RestClient(this, isNew);
            restClient.post("view", params, new RestAsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    super.onSuccess(statusCode, headers, responseBody);
                    try {
                        JSONObject car = data.getJSONArray("data").getJSONObject(0);
                        ((TextView) findViewById(R.id.car_marka)).setText(car.getString("car_marka") + " " + car.getString("car_model"));
                        ((TextView) findViewById(R.id.gos_nomer)).setText(car.getString("gos_nomer"));
                        carModelId = car.getString("car_model_id");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            findViewById(R.id.deleteButton).setVisibility(View.VISIBLE);
        } else
            findViewById(R.id.deleteButton).setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        finish();//go back to the previous Activity
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void onClickBack(View v) {
        finish();//go back to the previous Activity
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }


    public void onClickEditCarMarka(View v) {
        Intent intent = new Intent(this, CarMarkaActivity.class);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            return;
        }
        String carMarka = data.getStringExtra("carMarka");
        String carModel = data.getStringExtra("carModel");
        carModelId = data.getStringExtra("carModelId");

        TextView carMarkaText = (TextView) findViewById(R.id.car_marka);
        carMarkaText.setText(carMarka + " " + carModel);
    }

    public void onClickSave(View v) {
        TextView gosNomerText = (TextView) findViewById(R.id.gos_nomer);
        String gosNomer = gosNomerText.getText().toString();

        RequestParams params = new RequestParams();
        params.put("model", "polzovatel_car");
        params.put("is_new", isNew ? "1" : "0");
        params.put("car_id", carId);
        params.put("polzovatel_id", Utils.getUserId(CarEditActivity.this));
        params.put("car_model_id", carModelId);
        params.put("gos_nomer", gosNomer);
        RestClient restClient = new RestClient(CarEditActivity.this, isNew);
        restClient.post("update", params, new RestAsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Intent intent = new Intent();
                intent.putExtra("carSave", true);
                setResult(RESULT_OK, intent);
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
        });
    }

    public void onClickDelete(View v) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        RequestParams params = new RequestParams();
                        params.add("model", "polzovatel_car");
                        params.put("car_id", carId);
                        RestClient restClient = new RestClient(CarEditActivity.this, isNew);
                        restClient.post("delete", params, new RestAsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                if (Utils.getPref(getBaseContext()).contains("car_id")) {
                                    if (Utils.getPref(getBaseContext()).getString("car_id", "").contentEquals(carId)) {
                                        Utils.getPref(getBaseContext()).edit().remove("car_id").apply();
                                    }
                                }
                                Intent intent = new Intent();
                                intent.putExtra("carSave", true);
                                setResult(RESULT_OK, intent);
                                finish();
                                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                            }
                        });
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.delete_car)).setPositiveButton(getString(R.string.yes), dialogClickListener)
                .setNegativeButton(getString(R.string.no), dialogClickListener).show();
    }
}
