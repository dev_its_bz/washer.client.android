/*
 * Copyright (c) ITS 2014.
 *
 * www.its.bz
 * dev@its.bz
 */

package bz.its.washer.client;

import android.annotation.TargetApi;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.view.View;
import android.view.Window;

import com.loopj.android.http.RequestParams;


public class SettingsActivity extends PreferenceActivity {

    private static int prefs = R.xml.setting;

    @Override

    protected void onCreate(final Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        addPreferencesFromResource(prefs);

        Preference setting_service = this.findPreference("service_enable");
        Preference setting_objects_limit = this.findPreference("objects_limit");

        setting_service.setOnPreferenceChangeListener(new SettingsListener());       //слушатели для добавления данных в бд
        setting_objects_limit.setOnPreferenceChangeListener(new SettingsListener());

        /*try {
            getClass().getMethod("getFragmentManager");
            AddResourceApi11AndGreater();
        } catch (NoSuchMethodException e) { //Api < 11
            AddResourceApiLessThan11();
        }*/
    }

    @Override
    public void onBackPressed() {
        finish();//go back to the previous Activity
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @SuppressWarnings("deprecation")
    protected void AddResourceApiLessThan11() {
        addPreferencesFromResource(prefs);

        Preference setting_service = this.findPreference("service_enable");
        Preference setting_objects_limit = this.findPreference("objects_limit");

        setting_service.setOnPreferenceChangeListener(new SettingsListener());       //слушатели для добавления данных в бд
        setting_objects_limit.setOnPreferenceChangeListener(new SettingsListener());
    }

    @TargetApi(11)
    protected void AddResourceApi11AndGreater() {
        getFragmentManager().beginTransaction().replace(android.R.id.content,
                new PF()).commit();
    }

    public void onClickBack(View v) {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @TargetApi(11)
    public static class PF extends PreferenceFragment {
        @Override
        public void onCreate(final Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            addPreferencesFromResource(SettingsActivity.prefs);
        }
    }

    public class SettingsListener implements Preference.OnPreferenceChangeListener {
        @Override
        public boolean onPreferenceChange(Preference preference,
                                          Object newValue) {
            RequestParams params = new RequestParams();
            params.put(preference.getKey(), newValue.toString());
            SharedPreferences pref = Utils.getPref(getBaseContext());
            SharedPreferences.Editor prefs = pref.edit();
            prefs.putString(preference.getKey(), newValue.toString());
            prefs.apply();

            params.put("model", "polzovatel_nastroiki");
            RestClient restClient = new RestClient(SettingsActivity.this);
            restClient.post("update", params, new RestAsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, org.apache.http.Header[] headers, byte[] responseBody) {
                    super.onSuccess(statusCode, headers, responseBody);
                }
            });
            return true;
        }
    }
}
