package bz.its.washer.client;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;


public class ObjectListActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.activity_object_list);
        loadObjectsList();

        SharedPreferences pref = Utils.getPref(getBaseContext());
        if (!pref.contains("first_object_list")) {
            pref.edit().putString("first_object_list", "1").apply();
            final Dialog dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.coach_mark_object_list);
            dialog.setCanceledOnTouchOutside(true);
            //for dismissing anywhere you touch
            View masterView = dialog.findViewById(R.id.coach_mark_master_view);
            masterView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        }
    }

    private void loadObjectsList() {
        SharedPreferences pref = Utils.getPref(getBaseContext());
        String myLat = pref.getString("myLat", "");
        String myLon = pref.getString("myLon", "");
        String gorod_id = pref.getString("gorod_id", "1");
        String objects_limit = pref.getString("objects_limit", "10");
        String uslugi_id = pref.getString("uslugi_id_filter", "");
        String filters = pref.getString("filters", "");
        String car_id = pref.getString("car_id", "");
        RequestParams params = new RequestParams();
        params.add("myLat", myLat);
        params.add("myLon", myLon);


        params.put("uslugi", uslugi_id);
        params.add("model", "object");
        params.add("car_id", car_id);
        params.add("gorod_id", gorod_id);
        params.add("limit", objects_limit);
        params.add("filters", filters);

        RestClient restClient = new RestClient(this, true);
        restClient.post("list", params, new RestAsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        super.onSuccess(statusCode, headers, responseBody);

                        JSONArray records = null;
                        if (data != null) {
                            try {
                                records = data.getJSONArray("data");
                            } catch (JSONException e) {
                                e.printStackTrace();
                                //TODO: Нет массива
                                return;
                            }
                        }

                        //ФОрмируем карту элементов массива данных для листа
                        JSONObject record = null;
                        ArrayList<HashMap<String, String>> arrayMap = new ArrayList<HashMap<String, String>>();
                        for (int i = 0; i < records.length(); i++) {
                            try {
                                record = records.getJSONObject(i);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            HashMap<String, String> map = new HashMap<String, String>();
                            Iterator keys = record.keys();
                            while (keys.hasNext()) {
                                String key = (String) keys.next();
                                try {
                                    map.put(key, record.getString(key));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            arrayMap.add(map);
                        }


                        final ListView mListView = (ListView) findViewById(R.id.objectListView);
                        SimpleAdapter adapter = new ObjectListViewAdapter(mActivity, arrayMap,
                                R.layout.list_item_objects, null, null);
                        mListView.setAdapter(adapter);
                        mListView.setEmptyView(findViewById(android.R.id.empty));

                        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                int index = mListView.getFirstVisiblePosition();
                                SharedPreferences pref = Utils.getPref(getBaseContext());
                                pref.edit().putInt("listIndex", index).apply();

                                String objectId = ((TextView) view.findViewById(R.id.id)).getText().toString();
                                String cena = ((TextView) view.findViewById(R.id.cena)).getText().toString();
                                String dlitelnost = ((TextView) view.findViewById(R.id.dlitelnost)).getText().toString();

                                Intent objectInfo = new Intent(getApplicationContext(), ObjectInfoActivity.class);
                                pref = Utils.getPref(getBaseContext());
                                String car_id = pref.getString("car_id", "");

                                objectInfo.putExtra("object_id", objectId);
                                objectInfo.putExtra("cena", cena);
                                objectInfo.putExtra("dlitelnost", dlitelnost);
                                objectInfo.putExtra("car_id", car_id);
                                startActivityForResult(objectInfo, 1);
                            }
                        });

                        SharedPreferences pref = Utils.getPref(getBaseContext());
                        int index = pref.getInt("listIndex", 0);
                        try {
                            mListView.setSelection(index);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        super.onFailure(statusCode, headers, responseBody, error);
                    }
                }
        );
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();//go back to the previous Activity
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void onClickBack(View v) {
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();//go back to the previous Activity
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void onClickFilter(View v) {
        Intent selectFilters = new Intent(getApplicationContext(), SelectFiltersActivity.class);
        startActivityForResult(selectFilters, 1);
    }

    public void onClickUslugi(View v) {
        Intent uslugi = new Intent(getApplicationContext(), SelectUslugaActivity.class);
        startActivityForResult(uslugi, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            return;
        }

        if (data.hasExtra("objectSelect")) {
            Intent intent = new Intent();
            intent.putExtras(data);
            setResult(RESULT_OK, intent);
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            return;
        }

        if (data.hasExtra("filtersSelect") || data.hasExtra("favoriteChanged") || data.hasExtra("uslugiSelect")) {
            loadObjectsList();
        }
    }
}
