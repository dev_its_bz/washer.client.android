package bz.its.washer.client;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class ZapisListActivity extends Activity {

    SectionsPagerAdapter mSectionsPagerAdapter;
    ViewPager mViewPager;
    List<View> pages;
    Boolean isChanged = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zapisi_list);

        loadZapisi();
    }

    private void loadZapisi() {
        pages = new ArrayList<View>();

        LayoutInflater inflater = LayoutInflater.from(this);
        View page = inflater.inflate(R.layout.fragment_zapisi_list, null);
        pages.add(page);
        load_zapisi_active();

        page = inflater.inflate(R.layout.fragment_zapisi_list, null);
        pages.add(page);
        load_zapisi_inactive();


        mSectionsPagerAdapter = new SectionsPagerAdapter(pages);
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);
    }

    private void load_zapisi_inactive() {
        load_zapisi(pages.get(1), "zapis_inactive");
    }

    private void load_zapisi_active() {
        load_zapisi(pages.get(0), "zapis_active");
    }

    private void load_zapisi(View rootView, String model) {
        final ListView lv = (ListView) rootView.findViewById(R.id.zapisiListView);
        lv.setEmptyView(rootView.findViewById(android.R.id.empty));
        AdapterView.OnItemClickListener onZapisClick = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                String zapis_id = ((TextView) view.findViewById(R.id.id)).getText().toString();
                Intent intent = new Intent(ZapisListActivity.this, ZapisViewActivity.class);
                intent.putExtra("zapis_id", zapis_id);
                startActivityForResult(intent, 1);
            }
        };

        RequestParams params = new RequestParams();
        RestList zapisList = new RestList(this, rootView, R.id.zapisiListView, R.layout.list_item_zapisi, onZapisClick);
        try {
            zapisList.restLoadList(model, params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onClickBack(View v) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        if (isChanged) {
            Intent intent = new Intent();
            intent.putExtra("zapisChange", true);
            setResult(RESULT_OK, intent);
        }
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            return;
        }

        if (data.hasExtra("zapisDelete")) {
            isChanged = true;
            loadZapisi();
        }
    }

    public class SectionsPagerAdapter extends PagerAdapterMy {
        public SectionsPagerAdapter(List<View> pages) {
            super(pages);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.zapisi_list_title_section1).toUpperCase(l);
                case 1:
                    return getString(R.string.zapisi_list_title_section2).toUpperCase(l);
            }
            return null;
        }
    }
}
