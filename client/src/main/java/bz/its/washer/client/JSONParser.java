package bz.its.washer.client;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import static android.provider.Settings.Global.getString;

public class JSONParser {
    private JSONObject jObj = null;
    private JSONArray jArr = null;
    private String json = "";
    private boolean finished = false;


    // constructor
    public JSONParser() {
    }

    public JSONObject getJSONObject(final Activity activity, final String action, String model, RequestParams params) {

        SyncHttpClient client = new SyncHttpClient();
        client.setMaxRetriesAndTimeout(5, 1000);
        params = Utils.putAuthParams(params, activity);
        params.add("model", model);

        /*
        AsyncHttpClient client = new AsyncHttpClient();
        client.setMaxRetriesAndTimeout(10, 1);
        params = Utils.putAuthParams(params, activity);
        params.add("model", model);
        finished = false;

        client.post("http://washer.its.bz/api/" + action, params, new AsyncHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, org.apache.http.Header[] headers, byte[] responseBody, java.lang.Throwable error) {
                Toast.makeText(activity.getApplicationContext(), "Ошибка обработки данных!\n" + error, Toast.LENGTH_LONG).show();
                finished = true;
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // Making HTTP request
                try {
                    json = new String(responseBody, "utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                // try parse the string to a JSON object
                try {
                    jObj = new JSONObject(json);
                    jObj = jObj.getJSONObject("data");
                } catch (JSONException e) {
                    Log.e("JSON Parser", "Error parsing data " + e.join());
                }
                // return JSON String
                finished = true;
            }
        });

        while (!finished) {
        }
                */
        return jObj;

    }

    public JSONArray getJSONArray(final Activity activity, final String action, String model, RequestParams params) {
        AsyncHttpClient client = new AsyncHttpClient();
        params = Utils.putAuthParams(params, activity);
        params.add("model", model);
        finished = false;
        client.post(activity.getString(R.string.baseApiUrl) + action, params, new AsyncHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, org.apache.http.Header[] headers, byte[] responseBody, java.lang.Throwable error) {
                Toast.makeText(activity.getApplicationContext(), activity.getString(R.string.message_error_ocured) + error, Toast.LENGTH_LONG).show();
                finished = true;
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // Making HTTP request
                finished = false;
                try {
                    json = new String(responseBody, "utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                // try parse the string to a JSON object
                try {
                    jObj = new JSONObject(json);
                    jArr = jObj.getJSONArray("data");
                } catch (JSONException e) {
                    Log.e("JSON Parser", "Error parsing data " + e.toString());
                }
                // return JSON String
                finished = true;
            }
        });

        while (!finished) {
        }
        return jArr;
    }
}