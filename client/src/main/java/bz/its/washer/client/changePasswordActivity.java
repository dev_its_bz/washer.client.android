/*
 * Copyright (c) ITS 2014.
 *
 * www.its.bz
 * dev@its.bz
 */

package bz.its.washer.client;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import org.apache.http.Header;

import static bz.its.washer.client.Utils.md5;


public class ChangePasswordActivity extends Activity {

    private EditText verifyCode;
    private EditText password;
    private EditText repeatedPassword;
    private Button saveButton;
    private String verifyCodeValue = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.activity_change_password);

        verifyCode = (EditText) findViewById(R.id.new_password_code);
        password = (EditText) findViewById(R.id.new_password);
        repeatedPassword = (EditText) findViewById(R.id.new_password_repeat);
        saveButton = (Button) findViewById(R.id.saveButton);

        repeatedPassword.setVisibility(View.GONE);
        password.setVisibility(View.GONE);
        saveButton.setVisibility(View.GONE);


        verifyCodeValue = getIntent().getStringExtra("verifyCodeValue");

        verifyCode.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                final EditText verifyCode = (EditText) findViewById(R.id.new_password_code);
                if (md5(verifyCode.getText().toString()).contentEquals(ChangePasswordActivity.this.verifyCodeValue)) {
                    findViewById(R.id.wait_sms_hint).setVisibility(View.GONE);
                    ChangePasswordActivity.this.repeatedPassword.setVisibility(View.VISIBLE);
                    ChangePasswordActivity.this.password.setVisibility(View.VISIBLE);
                    ChangePasswordActivity.this.saveButton.setVisibility(View.VISIBLE);
                    ChangePasswordActivity.this.verifyCode.setVisibility(View.GONE);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    public void onClickSave(View v) {
        if (password.getText().toString().isEmpty() || repeatedPassword.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), getString(R.string.password_cannot_be_blank), Toast.LENGTH_LONG).show();
            return;
        }

        if (password.getText().toString().length() < 6 || repeatedPassword.getText().toString().length() < 6) {
            Toast.makeText(getApplicationContext(), getString(R.string.password_length_must_be), Toast.LENGTH_LONG).show();
            return;
        }

        if (!password.getText().toString().equals(repeatedPassword.getText().toString())) {
            Toast.makeText(getApplicationContext(), getString(R.string.passwords_are_not_equals), Toast.LENGTH_LONG).show();
            return;
        }

        RequestParams params = new RequestParams();
        params.put("login", getIntent().getStringExtra("phone"));
        params.put("newParol", md5(md5(password.getText().toString())));
        params.put("verifyCode", verifyCode.getText().toString());
        params.put("model", "polzovatel_password");

        RestClient restClient = new RestClient(this);
        restClient.post("update", params, new RestAsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                super.onSuccess(statusCode, headers, responseBody);
                Toast.makeText(getApplicationContext(), getString(R.string.password_successfully_changed), Toast.LENGTH_LONG).show();
                Intent intent = new Intent();
                intent.putExtra("passwordChanged", true);
                intent.putExtra("newPassword", password.getText().toString());
                setResult(RESULT_OK, intent);
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
        });
    }

    public void onClickBack(View v) {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
