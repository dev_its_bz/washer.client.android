/*
 * Copyright (c) ITS 2014.
 *
 * www.its.bz
 * dev@its.bz
 */

package bz.its.washer.client;

import android.app.Activity;
import android.view.View;
import android.widget.ExpandableListView;

import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Пользователь on 10.04.2014.
 */


public class RestServicesList {


    ExtendExpandableListAdapter adapter;
    ArrayList<UslugiCategory> category_array = new ArrayList<UslugiCategory>();
    private ExpandableListView elvMain;
    private Activity mActivity;
    private JSONArray service_groups = null;
    private JSONArray services = null;
    private ArrayList<String> Services = new ArrayList<String>();
    private ArrayList<String> Services_names = new ArrayList<String>();

    public RestServicesList(Activity activity, ExpandableListView lv) {
        mActivity = activity;
        elvMain = lv;
        Services = new ArrayList<String>();
    }

    public void selectItem(int serviceId) {
        for (UslugiCategory gr : category_array)
            for (SubCategory item : gr.subcategory_array)
                if (item.id == serviceId) {
                    item.selected = true;
                    Services.add(String.valueOf(item.id));
                    Services_names.add(String.valueOf(item.subcategory_name));
                }
        try {
            adapter.notifyDataSetChanged();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    public String getItemName(int id) {
        for (UslugiCategory gr : category_array)
            for (SubCategory item : gr.subcategory_array)
                if (item.id == id) {
                    return item.subcategory_name;
                }
        return null;
    }

    public void unsetAllItems() {
        Services.clear();
        Services_names.clear();
        for (UslugiCategory gr : category_array)
            gr.ClearAll();
    }

    public void DrawList() {

        RestClient restClient = new RestClient(mActivity);
        RequestParams params = new RequestParams();
        params.add("car_id", mActivity.getIntent().getStringExtra("car_id"));
        params.add("model", "service_tree_list");
        restClient.post("list", params, new RestAsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                super.onSuccess(statusCode, headers, responseBody);

                if (data == null)
                    return;
                try {
                    service_groups = data.getJSONArray("data").getJSONArray(1);
                    services = data.getJSONArray("data").getJSONArray(0);

                } catch (JSONException e) {
                    e.printStackTrace();
                    return;
                }

                // заполняем коллекцию групп из массива с названиями групп

                for (int i = 1; i < service_groups.length(); i++) {
                    try {
                        UslugiCategory category = new UslugiCategory();
                        category.category_name = service_groups.getJSONObject(i).getString("name");

                        int id_group = service_groups.getJSONObject(i).getInt("id");
                        for (int j = 0; j < services.length(); j++) {
                            if (services.getJSONObject(j).getInt("id_group") == id_group) {
                                SubCategory subcategory = new SubCategory();
                                subcategory.subcategory_name = services.getJSONObject(j).getString("name");
                                subcategory.id = services.getJSONObject(j).getInt("id");
                                category.subcategory_array.add(subcategory);
                            }
                        }
                        category_array.add(category);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }


                adapter = new ExtendExpandableListAdapter(mActivity, elvMain, category_array);
                elvMain.setAdapter(adapter);


                elvMain.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                    public boolean onChildClick(ExpandableListView parent, View v,
                                                int groupPosition, int childPosition, long id) {
                        if (category_array.get(groupPosition).subcategory_array.get(childPosition).selected) {
                            category_array.get(groupPosition).subcategory_array.get(childPosition).selected = false;
                            Services.remove(String.valueOf(category_array.get(groupPosition).subcategory_array.get(childPosition).id));
                            Services_names.remove((category_array.get(groupPosition).subcategory_array.get(childPosition).subcategory_name));

                        } else {
                            category_array.get(groupPosition).subcategory_array.get(childPosition).selected = true;
                            Services.add(String.valueOf(category_array.get(groupPosition).subcategory_array.get(childPosition).id));
                            Services_names.add((category_array.get(groupPosition).subcategory_array.get(childPosition).subcategory_name));
                        }
                        adapter.notifyDataSetChanged();
                        return true;
                    }
                });
                String uslugi_id = Utils.getPref(mContext).getString("uslugi_id_filter", "");
                if (!uslugi_id.contentEquals("")) {
                    RestServicesList.this.unsetAllItems();
                    for (Object id : Utils.toArray(uslugi_id)) {
                        RestServicesList.this.selectItem(Integer.valueOf(id.toString()));
                    }
                }
            }
        });
    }


    public ArrayList GetCheckedElementsId() {
        return Services;
    }

    public ArrayList GetCheckedElementsNames() {
        return Services_names;
    }
}
