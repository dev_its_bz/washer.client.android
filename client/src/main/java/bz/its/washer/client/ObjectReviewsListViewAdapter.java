package bz.its.washer.client;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by zolotarev on 13.03.14.
 */
public class ObjectReviewsListViewAdapter extends BaseAdapter {
    Context context;
    ArrayList<HashMap<String, String>> data;
    private static LayoutInflater inflater = null;

    public ObjectReviewsListViewAdapter(Context context, ArrayList<HashMap<String, String>> data) {
        // TODO Auto-generated constructor stub
        this.context = context;
        this.data = data;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub

        return data.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.list_item_reviews, null);
     /*   LoaderImageView avatar = (LoaderImageView) vi.findViewById(R.id.loaderImageView);
        switch (position) {
            case 1:
                if (!avatar.isLoaded)
                    avatar.setImageDrawable("http://sd.its.bz/images/users_photo/zolotarev.png");
                break;
            case 2:
                if (!avatar.isLoaded)
                    avatar.setImageDrawable("http://sd.its.bz/images/users_photo/pivnenko.png");
                break;
            case 3:
                if (!avatar.isLoaded)
                    avatar.setImageDrawable("http://sd.its.bz/images/users_photo/svetashov.png");
                break;
            case 4:
                if (!avatar.isLoaded)
                    avatar.setImageDrawable("http://sd.its.bz/images/users_photo/korenev.png");
                break;
            case 5:
                if (!avatar.isLoaded)
                    avatar.setImageDrawable("http://sd.its.bz/images/users_photo/shadrin.png");
                break;
            case 6:
                if (!avatar.isLoaded)
                    avatar.setImageDrawable("http://sd.its.bz/images/users_photo/kaidashov.png");
                break;
            default:
                if (!avatar.isLoaded)
                    avatar.setImageDrawable("http://sd.its.bz/images/users_photo/no_photo.png");
        } */

        TextView commentText = (TextView) vi.findViewById(R.id.review);
        commentText.setText(data.get(position).get("review").toString());

        TextView autorText = (TextView) vi.findViewById(R.id.avtor);
        autorText.setText(data.get(position).get("avtor").toString());

        TextView dateText = (TextView) vi.findViewById(R.id.data_sozdaniya);
        dateText.setText(data.get(position).get("data_sozdaniya").toString());

        RatingBar rb = (RatingBar) vi.findViewById(R.id.grade);
        rb.setRating(Integer.parseInt(data.get(position).get("grade").toString()));


        return vi;
    }
}
