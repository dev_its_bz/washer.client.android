/*
 * Copyright (c) ITS 2014.
 *
 * www.its.bz
 * dev@its.bz
 */

package bz.its.washer.client;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import org.apache.http.Header;

public class SendBugActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.activity_send_bug);
    }

    public void onClickSendBug(View v) {
        TextView bug_text = (TextView) findViewById(R.id.bug_text);
        if (bug_text.length() == 0) {
            Toast.makeText(getBaseContext(), getString(R.string.message_cannot_be_empty), Toast.LENGTH_LONG).show();
            return;
        }
        RequestParams params = new RequestParams();
        params.add("bugText", bug_text.getText().toString());
        params.add("model", "bug");
        RestClient restClient = new RestClient(this);
        restClient.post("create", params, new RestAsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                super.onSuccess(statusCode, headers, responseBody);
                Toast.makeText(SendBugActivity.this, getString(R.string.your_message_are_sended), Toast.LENGTH_LONG).show();
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();//go back to the previous Activity
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void onClickBack(View v) {
        finish();//go back to the previous Activity
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
