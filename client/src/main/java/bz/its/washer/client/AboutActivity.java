/*
 * Copyright (c) ITS 2014.
 *
 * www.its.bz
 * dev@its.bz
 */

package bz.its.washer.client;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class AboutActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.activity_about);

        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = pInfo.versionName;
        String build = String.valueOf(pInfo.versionCode);
        String build_date = "";

        try {
            ApplicationInfo ai = getPackageManager().getApplicationInfo(getPackageName(), 0);
            ZipFile zf = new ZipFile(ai.sourceDir);
            ZipEntry ze = zf.getEntry("classes.dex");
            long time = ze.getTime();
            build_date = getString(R.string.about_in_time) + SimpleDateFormat.getInstance().format(new java.util.Date(time));

        } catch (Exception e) {
            e.printStackTrace();
        }

        ((TextView) findViewById(R.id.version_text)).setText(getString(R.string.about_version) + " " + version + build_date);
        ((TextView) findViewById(R.id.build_text)).setText(getString(R.string.about_build) + " " + build);
    }

    public void onClickRateApp(View v) {
        Uri uri = Uri.parse("market://details?id=" + getApplicationContext().getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName())));
        }
    }

    public void onClickSendBug(View view) {
        Intent sendBug = new Intent(this, SendBugActivity.class);
        startActivity(sendBug);
    }

    public void onClickShowLic(View view) {
        Uri uri = Uri.parse(getString(R.string.licenseUrl));
        Intent goToUrl = new Intent(Intent.ACTION_VIEW, uri);
        try {
            startActivity(goToUrl);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.licenseUrl))));
        }
    }

    public void onClickSlides(View view) {
        Intent slides = new Intent(this, SlidesActivity.class);
        startActivity(slides);
    }

    @Override
    public void onBackPressed() {
        finish();//go back to the previous Activity
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void onClickBack(View v) {
        finish();//go back to the previous Activity
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
